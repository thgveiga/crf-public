package br.com.tveiga.log;

public enum Level {
	INFO, ERROR, DEBUG;
}
