package br.com.tveiga.log;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import br.com.tveiga.util.Directory;

public final class Log {

	private static final Logger logger = Logger.getLogger(Log.class);
	
	static {
		try {
			PropertyConfigurator.configure(Directory.LOG4J_PROPERTIES.getPath());
		}catch(Exception e){
			e.printStackTrace();
		}
	    
	}

	private Log() {
	}

	public static final void log(String className, int lineNumber, String message, Level level) {

		String msg = className + " - " + lineNumber + " - " + message;
		
		switch (level) {
		case INFO:
			logger.info(msg);
			break;

		case DEBUG:
			logger.debug(msg);
			break;

		case ERROR:
			logger.error(msg);
			break;
		}

	}

}
