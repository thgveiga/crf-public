package br.com.tveiga.main;

import javax.swing.SwingUtilities;
import javax.swing.UnsupportedLookAndFeelException;

import br.com.tveiga.log.Level;
import br.com.tveiga.log.Log;
import br.com.tveiga.ui.main.Login;

public final class Start {

	private Start(){
	}
	
	private final void startApp() throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException{
		Login login = new Login();
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				login.setVisible(true);	
			}
		});
		
	}
	
	public static void main(String[] args) {
			Start self = new Start();
			try {
				self.startApp();
			} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
				Log.log(Start.class.getName(), 29, e.getMessage(), Level.ERROR);
			}
	}
	
}
