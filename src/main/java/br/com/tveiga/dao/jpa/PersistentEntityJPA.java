package br.com.tveiga.dao.jpa;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;

import br.com.tveiga.bean.PersistentEntity;
import br.com.tveiga.dao.PersistentEntityDAO;
import br.com.tveiga.log.Level;
import br.com.tveiga.log.Log;
import br.com.tveiga.util.Settings;

public class PersistentEntityJPA<P extends PersistentEntity> implements PersistentEntityDAO<P> {

	protected Class<P> pClass;
	protected EntityManager manager;
	protected EntityTransaction transaction;
	protected P entity;

	protected PersistentEntityJPA(P p) {
		this.entity = p;
		this.pClass = (Class<P>) entity.getClass();
		String url = "jdbc:mysql://" + Settings.getInstance().getPropertie("host") + ":3306/crf"; 
		Map<String, String> properties = new HashMap<String, String>();
	    properties.put("javax.persistence.jdbc.url", url);

		EntityManagerFactory factory = Persistence.createEntityManagerFactory("persistenceUnit", properties);
		EntityManager manager = factory.createEntityManager();
		this.manager = manager;
		this.transaction = manager.getTransaction();
	}
	
	@Override
	public void create() {
		synchronized (entity) {
     	  entity.setCreatedAt(DateTime.now());
	      entity.setUpdatedAt(DateTime.now());
		  entity.setActive(true);
		
		  transaction.begin();
		  manager.persist(entity);
		  transaction.commit();
		}
	}

	@Override
	public void delete() {
		synchronized (entity) {
		  entity = manager.find(pClass, entity.getId());
		  transaction.begin();
		  entity.setUpdatedAt(DateTime.now());
		  entity.setActive(false);
		  transaction.commit();
		}
	}

	@Override
	public void update(P other) {
		synchronized (entity) {
		  entity = manager.find(pClass, entity.getId());
		  transaction.begin();
		  entity.setUpdatedAt(DateTime.now());
		  
			for(Field field : pClass.getDeclaredFields()){
				try {
				field.setAccessible(true);
				Object value = field.get(other);
				if(value != null && StringUtils.isNotBlank(value.toString()))
					field.set(entity, value);
				} catch (IllegalArgumentException | IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		  transaction.commit();
		}
	}

	private Query getQuery() throws IllegalArgumentException, IllegalAccessException{
		entity.setActive(true);
		StringBuilder stringBuilder = new StringBuilder("select p from ").append(pClass.getSimpleName()).append(" p where ");

		List<Field> fieldsValids = new ArrayList<Field>();
		for(Field field : pClass.getDeclaredFields()){
			field.setAccessible(true);
			Object object = field.get(entity);
			if(object != null && StringUtils.isNotBlank(object.toString()))
				fieldsValids.add(field);
		}

		for(int index = 0 ; index < fieldsValids.size(); index++){
			Field field = fieldsValids.get(index);
			stringBuilder.append(" p.").append(field.getName()).append(" = :").append(field.getName()).append(index < fieldsValids.size() - 1 ? " and " : " ");
		}

		Query query = manager.createQuery(stringBuilder.toString(),pClass);
		
		for(Field field : fieldsValids){
			Object object = field.get(entity);
			query.setParameter(field.getName(), object);
		}
		
		return query;
	}
	
	@Override
	public List<P> search() {
		List<P> result = null;
		
		try {
			result = (List<P>) getQuery().getResultList();
		} catch (IllegalArgumentException | IllegalAccessException e) {
			Log.log(PersistentEntityJPA.class.getSimpleName(), 122, e.getMessage(), Level.ERROR);
		}
		
		return result;
	}

	@Override
	public P frist() {
		List<P> list = all();
		if(list != null && !list.isEmpty())
			return list.get(0);
		
		return null;
	}

	@Override
	public P last() {
		List<P> list = all();
		if(list != null && !list.isEmpty())
			return list.get(list.size() -1);
		
		return null;
	}
	
	@Override
	public P searchOne() {
		P p = null;
		
		try {
			Query query = getQuery();
			query.setMaxResults(1);
			List<P> result = query.getResultList();
			if(result != null && !result.isEmpty())
				p = result.get(0);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			Log.log(PersistentEntityJPA.class.getSimpleName(), 153, e.getMessage(), Level.ERROR);
		}
		
		return p;
	}
	
	@Override
	public List<P> all() {
		List<P> resultList = manager.createQuery("Select p From " + pClass.getSimpleName() + " p where p.active = 1 ", pClass).getResultList();
		return resultList;
	}

	@Override
	public P next() {
		List<P> resultList = manager.createQuery("Select p From " + pClass.getSimpleName() + " p where p.active = 1 and p.id > " + entity.getId() + " order by id asc ", pClass).getResultList();
		return resultList != null && !resultList.isEmpty() ? resultList.get(0) : null;
	}

	@Override
	public P previous() {
		List<P> resultList = manager.createQuery("Select p From " + pClass.getSimpleName() + " p where p.active = 1 and p.id < " + entity.getId() + " order by id desc ", pClass).getResultList();
		return resultList != null && !resultList.isEmpty() ? resultList.get(0) : null;
	}

}
