package br.com.tveiga.dao.jpa;

import br.com.tveiga.bean.Usuario;
import br.com.tveiga.dao.UsuarioDAO;

public final class UsuarioJPA extends PersistentEntityJPA<Usuario> implements UsuarioDAO {
	
	public UsuarioJPA(Usuario usuario) {
		super(usuario);
	}

}
