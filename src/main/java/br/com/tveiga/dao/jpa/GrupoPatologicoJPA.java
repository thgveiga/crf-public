package br.com.tveiga.dao.jpa;

import br.com.tveiga.bean.GrupoPatologico;
import br.com.tveiga.dao.GrupoPatologicoDAO;

public final class GrupoPatologicoJPA extends  PersistentEntityJPA<GrupoPatologico> implements GrupoPatologicoDAO{

	public GrupoPatologicoJPA(GrupoPatologico grupoPatologico) {
		super(grupoPatologico);
		
	}
	

	
}
