package br.com.tveiga.dao;

import br.com.tveiga.bean.Medicamento;

public interface MedicamentoDAO extends PersistentEntityDAO<Medicamento>{
	
}
