package br.com.tveiga.dao;

import br.com.tveiga.bean.GrupoPatologico;

public interface GrupoPatologicoDAO extends PersistentEntityDAO<GrupoPatologico>{
	
}
