package br.com.tveiga.dao;

import java.util.List;

import br.com.tveiga.bean.PersistentEntity;

public interface PersistentEntityDAO<P extends PersistentEntity> {

	void create();
	
	void delete();
	
	void update(P other);
	
	P searchOne();
	
	List<P> search() throws IllegalArgumentException, IllegalAccessException;
	
	P frist();
	
	P next();
	
	P previous();
	
	P last();
	
	List<P>all();
}
