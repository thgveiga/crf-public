package br.com.tveiga.util;

import java.io.File;

public enum ButtonIcon {

	BUTTON_NEW("BUTTON_NEW", "button_new.png"), 
	BUTTON_EDIT("BUTTON_EDIT", "button_edit.png"), 
	BUTTON_EXCLUIR("BUTTON_EXCLUIR", "button_excluir.png"), 
	BUTTON_PREVIOUS("BUTTON_PREVIOUS", "button_previous.png"), 
	BUTTON_NEXT("BUTTON_NEXT", "button_next.png"), 
	BUTTON_BACK("BUTTON_BACK","button_back.png"), 
	BUTTON_ADD("BUTTON_ADD", "button_add.png"),
	BUTTON_SAVE("BUTTON_SAVE", "button_save.png"),
	BUTTON_PRINT("BUTTON_PRINT", "button_imprimir.png"),
	BUTTON_SELECT_ALL("BUTTON_SELECT_ALL", "button_select_all.png"),
	BUTTON_CANCEL("BUTTON_CANCEL", "button_cancelar.png"),
	CRF_IMAGE("CRF_IMAGE", "crfPageImage_2.png");
	
	private String name;
	private String path;

	private ButtonIcon(String name, String path) {
		this.name = name;
		this.path = Directory.IMG.getPath() + File.separator + "icones" + File.separator + path;
	}

	public String getName() {
		return name;
	}

	public String getPath() {
		return path;
	}

	@Override
	public String toString() {
		return "[ name : " + name + " path: " + path + " ]";
	}

}
