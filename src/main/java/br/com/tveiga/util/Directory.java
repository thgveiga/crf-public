package br.com.tveiga.util;

import java.io.File;

public enum Directory {

	    APP_SETTINGS("APP_SETTINGS", "app_settings"),
		LOG4J_PROPERTIES("LOG4J_PROPERTIES", "app_settings" + File.separator + "log4j.properties"),
		IMG("IMG", "app_settings" + File.separator + "img"),
		FORM("FORM", "app_settings" + File.separator + "form"),
	    HELP("HELP", "app_settings" + File.separator + "help"),
	    BACKUP("BACKUP", "app_settings" + File.separator + "backup");
	    
		private String name;
		private String path;

		private Directory(String name, String path) {
			this.name = name;
			this.path = path;
		}
		
		public String getName(){
		  return name;
		}
		
		public String getPath(){
			  return path;
		}
}
