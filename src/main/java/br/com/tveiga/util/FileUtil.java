package br.com.tveiga.util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import br.com.tveiga.log.Level;
import br.com.tveiga.log.Log;

public final class FileUtil {

	public static final List<String> getItensFromFile(final String file) {
		return getItensFromFile(file, true);
	}

	public static final List<String> getItensFromFile(final String file, final boolean commentsActive) {

		List<String> itens = new LinkedList<String>(); 
		try {
			String item = "";
			BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
			while ((item = bufferedReader.readLine()) != null)
				
				if(!commentsActive || !item.startsWith("#"))
					itens.add(item.replace("#", ""));
			
		} catch (FileNotFoundException e) {
			Log.log(FileUtil.class.getName() , 32 , "arquivo " + file + " nao encontrado", Level.ERROR);
		} catch (IOException e) {
			Log.log(FileUtil.class.getName() , 34, "arquivo " + file + e.getMessage() , Level.ERROR);
		}

		return itens;
	}
	
}
