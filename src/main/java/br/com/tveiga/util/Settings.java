package br.com.tveiga.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import br.com.tveiga.log.Level;
import br.com.tveiga.log.Log;

public final class Settings {

	private static Settings self;
	private static Properties properties = new Properties();
	private static File file;
	
	private Settings() {
		String propFile = Directory.APP_SETTINGS.getPath() + File.separator + "settings.properties";
		file = new File(propFile);
		
		if(!file.exists())
			Log.log(Settings.class.getSimpleName(), 26, "Arquivo nao encontrado " + file.getPath(), Level.ERROR);
		else
			readProperties();
	}
	
	public static final Settings getInstance(){
		if (self == null)
			synchronized (Settings.class) {
				if (self == null)
					self = new Settings();
			}
		return self;
	}
	
	public Properties getProperties(){
		return properties;
	}

	public void setProperties(Properties properties){
		this.properties = properties;
		writeProperties();
	}
	
	public String getPropertie(String name){
		return properties.getProperty(name);
	}
	
	public void setPropertie(String name, String value){
		properties.setProperty(name, value);
	}
	
	private void readProperties(){
			try {
				FileInputStream input = new FileInputStream(file);
				properties.load(input);
				input.close();
			} catch (IOException e) {
				Log.log(Settings.class.getSimpleName(), 66, e.getMessage(), Level.ERROR);
			}
		
	}
	
	private void writeProperties(){
		try {
			FileOutputStream output = new FileOutputStream(file);
			properties.store(output, null);
			output.flush();
			output.close();
		} catch (IOException e) {
			Log.log(Settings.class.getSimpleName(), 75, e.getMessage(), Level.ERROR);
		}

	}
}
