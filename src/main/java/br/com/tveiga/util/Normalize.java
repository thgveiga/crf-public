package br.com.tveiga.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.Normalizer;
import java.text.Normalizer.Form;

import org.apache.commons.lang.StringUtils;

import br.com.tveiga.log.Level;
import br.com.tveiga.log.Log;

public final class Normalize {

	private Normalize() {
	}
	
	public static final String normalizeName(String name){
		name = StringUtils.deleteWhitespace(name.toLowerCase());
		name = Normalizer.normalize(name, Form.NFD);
		name = name.replaceAll("[^\\p{ASCII}]", "").replaceAll("[\\p{Punct}]", "");
		return name;
	}
	
	public static final String toMD5(String text){
		MessageDigest md;
		String md5Text = null;
		try {
			md = MessageDigest.getInstance("MD5");
	        md.update(text.getBytes());
	        BigInteger hash = new BigInteger(1, md.digest());
	        md5Text = hash.toString(16);
		} catch (NoSuchAlgorithmException e) {
			Log.log(Normalize.class.getName() , 35 , e.getMessage(), Level.ERROR);
		}

		return md5Text;

	}

}
