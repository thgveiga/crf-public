package br.com.tveiga.factory;

import br.com.tveiga.bean.Paciente;
import br.com.tveiga.log.Level;
import br.com.tveiga.log.Log;
import br.com.tveiga.ui.cadastro.Cadastro1Consulta;
import br.com.tveiga.ui.cadastro.CadastroConsulta;
import br.com.tveiga.ui.cadastro.CadastroDadosDemograficos;
import br.com.tveiga.ui.cadastro.CadastroExameImagem;
import br.com.tveiga.ui.cadastro.CadastroExameLaboral;
import br.com.tveiga.ui.cadastro.CadastroHistorico;
import br.com.tveiga.ui.cadastro.CadastroInterConsulta;
import br.com.tveiga.ui.cadastro.CadastroMedicamento;
import br.com.tveiga.ui.cadastro.CadastroTab;

public final class CadastroTabFactory {

	private CadastroTab cadastroTab;
	private static CadastroTabFactory self;
	private static Paciente paciente;
	
	private CadastroTabFactory() {

	}

	public static final CadastroTabFactory getInstance(Paciente paciente) {
		if (self == null)
			synchronized (CadastroTabFactory.class) {
				if (self == null)
					CadastroTabFactory.paciente = paciente;
					self = new CadastroTabFactory();
			}
		return self;
	}

	public final CadastroTab getCadastroTab(final String find) {

		switch (find) {
		case "consulta":
			cadastroTab = new CadastroConsulta(paciente);
			break;
		case "dadosdemograficos":
			cadastroTab = new CadastroDadosDemograficos(paciente);
			break;
		case "historico":
			cadastroTab = new CadastroHistorico(paciente);
			break;
		case "medicamento":
			cadastroTab = new CadastroMedicamento(paciente);
			break;
		case "examelaboral":
			cadastroTab = new CadastroExameLaboral(paciente);
			break;
		case "exameimagem":
			cadastroTab = new CadastroExameImagem(paciente);
			break;			
		case "1consulta":
			cadastroTab = new Cadastro1Consulta(paciente);
			break;
		case "interconsultas":
			cadastroTab = new CadastroInterConsulta(paciente);
			break;
		default:
			Log.log(this.getClass().getName(), 81, "entidade " + find
					+ " nao encontrada ", Level.ERROR);
		}

		return cadastroTab;
	}

}
