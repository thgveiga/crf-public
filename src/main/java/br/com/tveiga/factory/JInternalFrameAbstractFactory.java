package br.com.tveiga.factory;

import br.com.tveiga.log.Level;
import br.com.tveiga.log.Log;

public final class JInternalFrameAbstractFactory {
	
	public enum MenuType {
		Cadastro("cadastro"), Pesquisa("pesquisa");
		
		private String name;
		
		private MenuType(final String name) {
			this.name = name;
		}
	}
	
	private JInternalFrameAbstractFactory() {
	}
	
	public static final JInternalFrameFactory getJInternalFrameFactory(MenuType type){
		JInternalFrameFactory internalFrameFactory = null;
		
		switch (type) {
		  case Cadastro:
			  internalFrameFactory = JInternalFrameCadastroFactory.getInstance();
			break;
			
          case Pesquisa:
			 internalFrameFactory = JInternalFramePesquisaFactory.getInstance();
			break;
		  default:
			Log.log(JInternalFrameAbstractFactory.class.getName(), 33, "nao foi possivel localizar o tipo " + type, Level.ERROR);
		}
		return internalFrameFactory;
	}
	
	public static final MenuType getByName(final String name){
		for (MenuType menuType : MenuType.values())
			if(name.equals(menuType.name))
				return menuType;
			
		return null;
	}

}
