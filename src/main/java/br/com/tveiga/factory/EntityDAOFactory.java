package br.com.tveiga.factory;

import br.com.tveiga.bean.ClasseTerapeutica;
import br.com.tveiga.bean.Consulta;
import br.com.tveiga.bean.Disciplina;
import br.com.tveiga.bean.ExameImagem;
import br.com.tveiga.bean.ExameLaboral;
import br.com.tveiga.bean.GrupoPatologico;
import br.com.tveiga.bean.Historico;
import br.com.tveiga.bean.Medicamento;
import br.com.tveiga.bean.Paciente;
import br.com.tveiga.bean.Patologia;
import br.com.tveiga.bean.Permissao;
import br.com.tveiga.bean.PersistentEntity;
import br.com.tveiga.bean.PrincipioAtivo;
import br.com.tveiga.bean.Usuario;
import br.com.tveiga.dao.ClasseTerapeuticaDAO;
import br.com.tveiga.dao.ConsultaDAO;
import br.com.tveiga.dao.DisciplinaDAO;
import br.com.tveiga.dao.ExameImagemDAO;
import br.com.tveiga.dao.ExameLaboralDAO;
import br.com.tveiga.dao.GrupoPatologicoDAO;
import br.com.tveiga.dao.HistoricoDAO;
import br.com.tveiga.dao.MedicamentoDAO;
import br.com.tveiga.dao.PacienteDAO;
import br.com.tveiga.dao.PatologiaDAO;
import br.com.tveiga.dao.PermissaoDAO;
import br.com.tveiga.dao.PersistentEntityDAO;
import br.com.tveiga.dao.PrincipioAtivoDAO;
import br.com.tveiga.dao.UsuarioDAO;
import br.com.tveiga.dao.jpa.ClasseTerapeuticaJPA;
import br.com.tveiga.dao.jpa.ConsultaJPA;
import br.com.tveiga.dao.jpa.DisciplinaJPA;
import br.com.tveiga.dao.jpa.ExameImagemJPA;
import br.com.tveiga.dao.jpa.ExameLaboralJPA;
import br.com.tveiga.dao.jpa.GrupoPatologicoJPA;
import br.com.tveiga.dao.jpa.HistoricoJPA;
import br.com.tveiga.dao.jpa.MedicamentoJPA;
import br.com.tveiga.dao.jpa.PacienteJPA;
import br.com.tveiga.dao.jpa.PatologiaJPA;
import br.com.tveiga.dao.jpa.PermissaoJPA;
import br.com.tveiga.dao.jpa.PrincipioAtivoJPA;
import br.com.tveiga.dao.jpa.UsuarioJPA;
import br.com.tveiga.log.Level;
import br.com.tveiga.log.Log;

public final class EntityDAOFactory {

	
	private EntityDAOFactory() {
	}
	
	public static final PersistentEntityDAO<? extends PersistentEntity> getEntityDAO(PersistentEntity persistentEntity){
		PersistentEntityDAO<? extends PersistentEntity> persistentEntityDAO = null;
		
		if(persistentEntity instanceof Paciente){
			 persistentEntityDAO = (PacienteDAO) new PacienteJPA((Paciente) persistentEntity);
		}else if(persistentEntity instanceof Disciplina){
			 persistentEntityDAO = (DisciplinaDAO) new DisciplinaJPA((Disciplina) persistentEntity);
		}else if(persistentEntity instanceof Consulta){
			 persistentEntityDAO = (ConsultaDAO) new ConsultaJPA((Consulta) persistentEntity);
		}else if(persistentEntity instanceof GrupoPatologico){
			 persistentEntityDAO = (GrupoPatologicoDAO) new GrupoPatologicoJPA((GrupoPatologico) persistentEntity);
		}else if(persistentEntity instanceof Medicamento){
			 persistentEntityDAO = (MedicamentoDAO) new MedicamentoJPA((Medicamento) persistentEntity);
		}else if(persistentEntity instanceof Patologia){
			 persistentEntityDAO = (PatologiaDAO) new PatologiaJPA((Patologia) persistentEntity);
		}else if(persistentEntity instanceof ClasseTerapeutica){
			 persistentEntityDAO = (ClasseTerapeuticaDAO) new ClasseTerapeuticaJPA((ClasseTerapeutica) persistentEntity);
		}else if(persistentEntity instanceof Historico){
			 persistentEntityDAO = (HistoricoDAO) new HistoricoJPA((Historico) persistentEntity);
		}else if(persistentEntity instanceof PrincipioAtivo){
			 persistentEntityDAO = (PrincipioAtivoDAO) new PrincipioAtivoJPA((PrincipioAtivo) persistentEntity);
		}else if(persistentEntity instanceof Usuario){
			 persistentEntityDAO = (UsuarioDAO) new UsuarioJPA((Usuario) persistentEntity);
		}else if(persistentEntity instanceof Permissao){
			 persistentEntityDAO = (PermissaoDAO) new PermissaoJPA((Permissao) persistentEntity);
		}else if(persistentEntity instanceof ExameLaboral){
			 persistentEntityDAO = (ExameLaboralDAO) new ExameLaboralJPA((ExameLaboral) persistentEntity);
		}else if(persistentEntity instanceof ExameImagem){
			 persistentEntityDAO = (ExameImagemDAO) new ExameImagemJPA((ExameImagem) persistentEntity);
		}else{
			Log.log(EntityDAOFactory.class.getName(), 83, "nao foi possivel criar a instancia de " + persistentEntity.getClass().getName(), Level.ERROR);
		}
		
		return persistentEntityDAO;
	}

}
