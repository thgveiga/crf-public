package br.com.tveiga.factory;

import javax.swing.JInternalFrame;

import br.com.tveiga.log.Level;
import br.com.tveiga.log.Log;
import br.com.tveiga.ui.pesquisa.PesquisaClasseTerapeutica;
import br.com.tveiga.ui.pesquisa.PesquisaConsulta;
import br.com.tveiga.ui.pesquisa.PesquisaDisciplina;
import br.com.tveiga.ui.pesquisa.PesquisaExameImagem;
import br.com.tveiga.ui.pesquisa.PesquisaExameLaboral;
import br.com.tveiga.ui.pesquisa.PesquisaGrupoPatologico;
import br.com.tveiga.ui.pesquisa.PesquisaHistorico;
import br.com.tveiga.ui.pesquisa.PesquisaMedicamento;
import br.com.tveiga.ui.pesquisa.PesquisaPaciente;
import br.com.tveiga.ui.pesquisa.PesquisaPatologia;
import br.com.tveiga.ui.pesquisa.PesquisaPermissao;
import br.com.tveiga.ui.pesquisa.PesquisaPrincipioAtivo;
import br.com.tveiga.ui.pesquisa.PesquisaUsuario;
import br.com.tveiga.util.Normalize;

public final class JInternalFramePesquisaFactory implements JInternalFrameFactory {

	private JInternalFramePesquisaFactory() {
	}

	private static JInternalFramePesquisaFactory self;
	
	@Override
	public final JInternalFrame getJInternalFrame(final String itemName) {
		JInternalFrame internalFrame = null;
		final String find = Normalize.normalizeName(itemName);
		switch (find) {
		  case "paciente":
			internalFrame = new PesquisaPaciente();
			break;
		  case "disciplina":
			internalFrame = new PesquisaDisciplina();
			break;
		  case "consulta":
				internalFrame = new PesquisaConsulta();
				break;
		  case "grupopatologico":
				internalFrame = new PesquisaGrupoPatologico();
				break;
		  case "medicamento":
				internalFrame = new PesquisaMedicamento();
				break;
		  case "patologia":
				internalFrame = new PesquisaPatologia();
				break;
		  case "classeterapeutica":
				internalFrame = new PesquisaClasseTerapeutica();
				break;
		  case "historico":
				internalFrame = new PesquisaHistorico();
				break;
		  case "principioativo":
				internalFrame = new PesquisaPrincipioAtivo();
				break;
		  case "usuario":
				internalFrame = new PesquisaUsuario();
				break;
		  case "permissao":
				internalFrame = new PesquisaPermissao();
				break;
		  case "examelaboral":
				internalFrame = new PesquisaExameLaboral();
				break;		
		  case "exameimagem":
				internalFrame = new PesquisaExameImagem();
				break;
		  default:
			Log.log(this.getClass().getName(), 74, "entidade " + itemName + " nao encontrada ", Level.ERROR);
		  }
		
		return internalFrame;
	}

	public static final JInternalFrameFactory getInstance() {
		if(self == null)
			synchronized (JInternalFramePesquisaFactory.class){ 
				if(self == null)
					self = new JInternalFramePesquisaFactory();
			}
		return self;
	}
	
}
