package br.com.tveiga.factory;

import javax.swing.JInternalFrame;

public interface JInternalFrameFactory {

	JInternalFrame getJInternalFrame(final String itemName);
}
