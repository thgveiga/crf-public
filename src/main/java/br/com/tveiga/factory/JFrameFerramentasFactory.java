package br.com.tveiga.factory;

import javax.swing.JFrame;

import br.com.tveiga.log.Level;
import br.com.tveiga.log.Log;
import br.com.tveiga.ui.ferramentas.ConfiguracoesFrame;
import br.com.tveiga.ui.ferramentas.EfetuarBackupFrame;
import br.com.tveiga.ui.ferramentas.RestaurarBackupFrame;
import br.com.tveiga.util.Normalize;

public final class JFrameFerramentasFactory {

	private static JFrameFerramentasFactory self;
	
	public static final JFrameFerramentasFactory getInstance() {
		if(self == null)
			synchronized (JFrameFerramentasFactory.class){ 
				if(self == null)
					self = new JFrameFerramentasFactory();
			}
		return self;
	}
	
	public final JFrame getJFrame(final String itemName) {
		JFrame jFrame = null;
			switch (Normalize.normalizeName(itemName)) {
			case "configuracoes":
				jFrame = new ConfiguracoesFrame();
			break;
			case "restaurarbackup":
				jFrame = new RestaurarBackupFrame();
			break;
			case "efetuarbackup":
				jFrame = new EfetuarBackupFrame();
			break;
			default:
				Log.log(this.getClass().getSimpleName(), 37, "frame " + itemName +" nao encontrado", Level.ERROR);
				break;
			}
			
		return jFrame;
	}
	
}
