package br.com.tveiga.factory;

import javax.swing.JInternalFrame;

import br.com.tveiga.log.Level;
import br.com.tveiga.log.Log;
import br.com.tveiga.ui.cadastro.CadastroPaciente;
import br.com.tveiga.ui.cadastro.CadastroUsuario;
import br.com.tveiga.util.Normalize;

public final class JInternalFrameCadastroFactory implements JInternalFrameFactory{

	private static JInternalFrameCadastroFactory self;

	private JInternalFrameCadastroFactory() {
	}

	@Override
	public final JInternalFrame getJInternalFrame(final String itemName) {
		JInternalFrame internalFrame = null;
		final String find = Normalize.normalizeName(itemName);
		
		switch (find) {
		  case "paciente":
			internalFrame = new CadastroPaciente();
			break;
		  case "usuario":
				internalFrame = new CadastroUsuario();
				break;
		  default:
			Log.log(this.getClass().getName(), 75,"entidade " + itemName + " nao encontrada ", Level.ERROR);
		  }
		
		return internalFrame;
	}
	
	public static final JInternalFrameCadastroFactory getInstance() {
		if(self == null)
			synchronized (JInternalFrameCadastroFactory.class){ 
				if(self == null)
					self = new JInternalFrameCadastroFactory();
			}
		return self;
	}
	
}
