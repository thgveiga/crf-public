package br.com.tveiga.ui.pesquisa;

import javax.swing.JInternalFrame;

public class PesquisaFrame extends JInternalFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name;
	
	public PesquisaFrame(String name) {
		super("Pesquisa  " + name);
		this.name = name;
	}

}
