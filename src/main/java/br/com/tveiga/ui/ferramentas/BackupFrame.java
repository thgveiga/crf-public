package br.com.tveiga.ui.ferramentas;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.filechooser.FileFilter;

import br.com.tveiga.log.Level;
import br.com.tveiga.log.Log;
import br.com.tveiga.ui.main.SobreFrame;
import br.com.tveiga.util.Directory;
import br.com.tveiga.util.Settings;

public abstract class BackupFrame extends JFrame {

	protected File selectedFile;
	private String nome;
	
	protected String host;
	protected String user;
	protected String password;
	protected Settings settings;
	
	public BackupFrame(String nome) {
		
		  super(nome);
		  this.nome = nome;
	
		  settings = Settings.getInstance();
          host = settings.getPropertie("host");
		  user = "";
		  password = "";

		  createLayout();
	      setResizable(false);
		  setLocationRelativeTo(getParent());
		  
	      try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		  } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
			Log.log(SobreFrame.class.getSimpleName(), 39, e.getMessage(), Level.ERROR);
		  }

	}

	private void createLayout() {
		
		JPanel jPanel = new JPanel(new BorderLayout());
		JPanel jPanelDiretorio = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JTextField jTextFieldArquivo = new JTextField(40);
		jTextFieldArquivo.setEditable(false);
		JButton  jButtonEscolher = new JButton("...");
		JFileChooser jFileChooser = new JFileChooser(Directory.BACKUP.getPath());
		jFileChooser.setDialogTitle(nome);
		jFileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		jFileChooser.setFileFilter(new FileFilter() {
			
			@Override
			public String getDescription() {
				return ".sql";
			}
			
			@Override
			public boolean accept(File f) {
				String nome = f.getPath();
				return nome.endsWith(".sql");
			}
		});
		
		jButtonEscolher.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int choise = jFileChooser.showDialog(BackupFrame.this, nome);
				
				if(choise == JFileChooser.APPROVE_OPTION){
					selectedFile = jFileChooser.getSelectedFile();
					jTextFieldArquivo.setText(selectedFile.getPath());
				}
			}
		});
		
		jPanelDiretorio.add(new JLabel("Arquivo: "));
		jPanelDiretorio.add(jTextFieldArquivo);
		jPanelDiretorio.add(jButtonEscolher);
		
		JPanel jPanelBotao = new JPanel(new FlowLayout(FlowLayout.LEFT));
		
		JButton backup = new JButton(nome);
		backup.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				backupAction();
			}
		});
		JButton cancelar = new JButton("Cancelar");
		cancelar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		jPanelBotao.add(backup);
		jPanelBotao.add(cancelar);
		
		jPanel.add(jPanelDiretorio, BorderLayout.NORTH);
		jPanel.add(jPanelBotao, BorderLayout.CENTER);
		add(jPanel);
		pack();
  }

	protected abstract void backupAction();

}
