package br.com.tveiga.ui.ferramentas;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.apache.commons.lang.StringUtils;

import br.com.tveiga.log.Level;
import br.com.tveiga.log.Log;
import br.com.tveiga.ui.main.SobreFrame;
import br.com.tveiga.util.Settings;

public class ConfiguracoesFrame extends JFrame {

	private Settings setting;
	
	public ConfiguracoesFrame() {
	
		super("Configuracoes");
		  setting = Settings.getInstance();
	      setResizable(false);
		  createLayout();
		  setLocationRelativeTo(getParent());
		  try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		  } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
			Log.log(SobreFrame.class.getSimpleName(), 22, e.getMessage(), Level.ERROR);
		  }
	}

	private void createLayout() {

		JPanel jPanel = new JPanel(new GridLayout(4, 1));
		
		JPanel jPanelDiretorio = new JPanel(new FlowLayout(FlowLayout.LEFT));
		jPanelDiretorio.add(new JLabel("Host/Ip :         "));
		JTextField jTextFieldHost = new JTextField(setting.getPropertie("host"), 25);
		jPanelDiretorio.add(jTextFieldHost);
		
		JPanel jPanelDump = new JPanel(new FlowLayout(FlowLayout.LEFT));
		jPanelDump.add(new JLabel("MysqlDump :   "));
		JTextField jTextFieldDump = new JTextField(setting.getPropertie("dump"), 25);
		jPanelDump.add(jTextFieldDump);
		
		JPanel jPanelImport = new JPanel(new FlowLayout(FlowLayout.LEFT));
		jPanelImport.add(new JLabel("MysqlImport : "));
		JTextField jTextFieldImport = new JTextField(setting.getPropertie("import"), 25);
		jPanelImport.add(jTextFieldImport);
		
		JPanel jPanelBotao = new JPanel(new FlowLayout(FlowLayout.LEFT));
		
		JButton ok = new JButton("OK");
		ok.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				
				Properties properties = new Properties();
				String host = jTextFieldHost.getText();
				String dump = jTextFieldDump.getText();
				String mysqlImport = jTextFieldImport.getText();
				
				properties.setProperty("host", StringUtils.isBlank(host) ? "localhost" : host);
				properties.setProperty("dump", StringUtils.isBlank(dump) ? "C:\\Program Files\\MySQL\\MySQL Server 5.5\\bin\\mysqldump.exe" : dump);
				properties.setProperty("import", StringUtils.isBlank(mysqlImport) ? "C:\\Program Files\\MySQL\\MySQL Server 5.5\\bin\\mysqlimport.exe" : mysqlImport);
				
				setting.setProperties(properties);
				
				int choise = JOptionPane.showOptionDialog(null, "Reiniciar sistema para atualizar configuracoes ?", " Configuracoes", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, JOptionPane.NO_OPTION);
				if (JOptionPane.YES_OPTION == choise) {
					System.gc();
					System.exit(0);
				}else{
					dispose();
				}
			}
		});
	
		JButton cancelar = new JButton("Cancelar");
		cancelar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		
		jPanelBotao.add(ok);
		jPanelBotao.add(cancelar);
	
		jPanel.add(jPanelDiretorio);
		jPanel.add(jPanelDump);
		jPanel.add(jPanelImport);
		jPanel.add(jPanelBotao);
		
		add(jPanel);
		
		pack();
	}
}
