package br.com.tveiga.ui.ferramentas;

import java.awt.Cursor;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.swing.JOptionPane;
import javax.swing.JProgressBar;

import br.com.tveiga.log.Level;
import br.com.tveiga.log.Log;

public class EfetuarBackupFrame extends BackupFrame {

	public EfetuarBackupFrame() {
		super("Efetuar Backup");
	}

	@Override
	protected void backupAction() {
		if(selectedFile == null)
			JOptionPane.showMessageDialog(null, "Sem arquivo para backup", null, JOptionPane.ERROR_MESSAGE);
		else{
			
			String nome = selectedFile.getPath();
			
			if(!nome.contains(".sql"))
				nome += ".sql";
			
			selectedFile = new File(nome);

			if(selectedFile.exists())
				selectedFile.delete();
			
			String dump = settings.getPropertie("dump");
			Runtime runtime = Runtime.getRuntime();
			String command = dump + " --user " + user + " --password "+ password + " --host " + host + " > " + selectedFile.getPath();
			try {
				selectedFile.createNewFile();
				this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
				Process p = runtime.getRuntime().exec(command);
				InputStream inputStream = p.getInputStream();
				int size = inputStream.available();
				JProgressBar progressBar = new JProgressBar(0, size);
				progressBar.setStringPainted(true);
				add(progressBar);
			} catch (IOException e) {
				Log.log(EfetuarBackupFrame.class.getSimpleName(), 26, e.getMessage(), Level.ERROR);
			}
				
		}
	}
}
