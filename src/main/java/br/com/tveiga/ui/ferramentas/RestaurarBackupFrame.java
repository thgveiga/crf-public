package br.com.tveiga.ui.ferramentas;

import java.awt.Cursor;
import java.io.IOException;

import javax.swing.JOptionPane;

import br.com.tveiga.log.Level;
import br.com.tveiga.log.Log;

public class RestaurarBackupFrame extends BackupFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public RestaurarBackupFrame() {
		super("Restaurar Backup");
	}

	@Override
	protected void backupAction() {
		if(selectedFile == null || !selectedFile.getPath().endsWith(".sql"))
			JOptionPane.showMessageDialog(null, "Arquivo de backup invalido", null, JOptionPane.ERROR_MESSAGE);
		else{
			String importSql = settings.getPropertie("import");
			Runtime runtime = Runtime.getRuntime();
			String command = importSql + " --user " + user + " --password "+ password + " --host " + host + " < " + selectedFile.getPath();
			try {
				this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
				Process p = runtime.getRuntime().exec(command);
			} catch (IOException e) {
				Log.log(EfetuarBackupFrame.class.getSimpleName(), 26, e.getMessage(), Level.ERROR);
			}
				
		}
	}
}
