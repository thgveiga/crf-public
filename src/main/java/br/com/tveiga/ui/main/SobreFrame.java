package br.com.tveiga.ui.main;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import br.com.tveiga.log.Level;
import br.com.tveiga.log.Log;
import br.com.tveiga.util.ButtonIcon;
import br.com.tveiga.util.Settings;

public final class SobreFrame  extends JFrame {

	public SobreFrame() {
      super("Sobre");
      
      setResizable(false);
	  createContent();
	  setLocationRelativeTo(getParent());
      try {
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
	  } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
		Log.log(SobreFrame.class.getSimpleName(), 29, e.getMessage(), Level.ERROR);
	  }
		
   }
	
	private void createContent(){
		
		Settings settings = Settings.getInstance();
		String nomeMedico =  settings.getPropertie("nomeMedico");;
		String serialNumber = "";
		Integer hostNumber = 0;
		
		JPanel panelGrid = new JPanel( new GridLayout(0, 1));
		
		String[] info = {"Ambulatorio de Especialidades da Gastroenterologia", 
				         "versao 1.0.0",
				         "Desenvolvido para: " + nomeMedico,
				         "Numero de serie : " + serialNumber,
				         "Numero de hosts : " + hostNumber,
				         "Copyright Todos os direitos reservados",
				         "TVeiga Solucoes Tecnologicas em Saude"
		};
		
		for(String t : info){
			JLabel jLabel = new JLabel(t);
			JPanel jPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
			jPanel.add(jLabel);
			panelGrid.add(jPanel);
		}
		
		add(new JLabel(new ImageIcon(ButtonIcon.CRF_IMAGE.getPath())), BorderLayout.NORTH);
		add(panelGrid, BorderLayout.CENTER);
		
		pack();
	}
}
