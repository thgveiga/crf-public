package br.com.tveiga.ui.main;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.apache.commons.lang.StringUtils;

import br.com.tveiga.bean.Usuario;
import br.com.tveiga.business.UsuarioBusiness;
import br.com.tveiga.log.Level;
import br.com.tveiga.log.Log;
import br.com.tveiga.util.ButtonIcon;

public final class Login extends JFrame{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final static String title = "Ambulatorio de Especialidades da Gastroenterologia - Login";
	private JFrame frameLoad;
	private JProgressBar progressBar = new JProgressBar(JProgressBar.HORIZONTAL);
	private Thread loadThread;
	
	public Login() throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
		super(title);
		
		setResizable(false);
		setMinimumSize(new Dimension(400,100));
		
		createLoading();
		Point p = frameLoad.getLocation();
		setLocation((p.x - frameLoad.getSize().width /2) , (p.y + frameLoad.getSize().height / 2));
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		
		creatLayot();
	}
	
	private void createLoading() {
		
		if(frameLoad == null){
			
			frameLoad = new JFrame();
			frameLoad.setLocationRelativeTo(null);
			frameLoad.setResizable(false);
			frameLoad.setMinimumSize(new Dimension(400,250));
			
			JLabel jLabel = new JLabel(new ImageIcon(ButtonIcon.CRF_IMAGE.getPath()));
			JPanel jPanel = new JPanel(new BorderLayout());
			jPanel.add(jLabel, BorderLayout.NORTH);
			jPanel.add(progressBar, BorderLayout.SOUTH);
			
			frameLoad.add(jPanel);
			
			try {
				UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
				Log.log(Login.class.getSimpleName(), 80, e.getMessage(), Level.ERROR);
			}
			
			frameLoad.pack();
			frameLoad.setVisible(true);
			
			frameLoad.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			
		}
	}

	private final void startLoading(boolean continua){
			
		if(continua){
			if(!frameLoad.isVisible())
				frameLoad.setVisible(true);
			
			loadThread = new Thread(new Runnable() {
				@Override
				public void run() {
					int i = 0;
					setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
					while(continua){
						try {
							Thread.sleep(100);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						++i;
						progressBar.setValue(i);
					}
					setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
				}});
			
			loadThread.start();
			
		}else{
			loadThread.stop();
			progressBar.setValue(0);
			frameLoad.dispose();
		}
	}
	
	private final boolean isValidLogin(String userName, String password){
		if(StringUtils.isBlank(userName) || StringUtils.isBlank(password))
			return false;

		Usuario usuario = new Usuario();
		usuario.setNome(userName);
		usuario.setSenha(password);
		
		return new UsuarioBusiness().login(usuario);
	}

	private final void creatLayot(){
		JPanel jPanel = new JPanel(new GridLayout(3,1));
		
		JPanel jPanelUsuario = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JPanel jPanelSenha = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JPanel jPanelButton = new JPanel(new FlowLayout(FlowLayout.LEFT));
		
		JLabel jLabelNome = new JLabel("Usuario: ");
		JTextField jTextFieldNome = new JTextField();
		jTextFieldNome.setColumns(50);
		jTextFieldNome.setFocusable(true);
		
		JLabel jLabelSenha = new JLabel("Senha:   ");
		JPasswordField jPasswordFieldSenha = new JPasswordField();
		jPasswordFieldSenha.setColumns(30);
		jPasswordFieldSenha.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyChar()==KeyEvent.VK_ENTER){
					startLoading(true);
					String password = String.valueOf(jPasswordFieldSenha.getPassword());
					openNewWindow(jTextFieldNome.getText(), password);
				}
			}
		});
		
		JButton buttonOK = new JButton("OK");
		
		buttonOK.addActionListener(buttonOKAction(jTextFieldNome, jPasswordFieldSenha));
		
		JButton buttonCancelar = new JButton("Cancelar");
		buttonCancelar.addActionListener(buttonCancelarAction());
		
		jPanelUsuario.add(jLabelNome);
		jPanelUsuario.add(jTextFieldNome);
		
		jPanelSenha.add(jLabelSenha);
		jPanelSenha.add(jPasswordFieldSenha);
		
		jPanelButton.add(buttonOK);
		jPanelButton.add(buttonCancelar);

		
		jPanel.add(jPanelUsuario);
		jPanel.add(jPanelSenha);
		jPanel.add(jPanelButton);
		
		add(jPanel);
		pack();
		
	}

	private final void openNewWindow(String userName, String password){
		dispose();
		if(isValidLogin(userName, password)){
			MainWindow mainWindow;
			try {
				mainWindow = new MainWindow();
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						startLoading(false);
						mainWindow.setVisible(true);
					}
				});
			} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e1) {
				Log.log(Login.class.getName() , 119,  e1.getMessage(), Level.ERROR);
			}
			
		}else{
			startLoading(false);
			JOptionPane.showMessageDialog(null, "Usuario ou senha incorretos", " Login erro", JOptionPane.OK_OPTION);
			setVisible(true);
		}
	}
	
	private final ActionListener buttonOKAction(JTextField nome, JPasswordField senha) {
		return new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				startLoading(true);
				String password = String.valueOf(senha.getPassword());
				openNewWindow(nome.getText(), password);
			}
		};
	}

	private final ActionListener buttonCancelarAction() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.gc();
				System.exit(0);
			}
		};
	}

}
