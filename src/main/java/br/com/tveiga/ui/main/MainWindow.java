package br.com.tveiga.ui.main;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;

import br.com.tveiga.factory.JFrameFerramentasFactory;
import br.com.tveiga.factory.JInternalFrameAbstractFactory;
import br.com.tveiga.factory.JInternalFrameAbstractFactory.MenuType;
import br.com.tveiga.factory.JInternalFrameFactory;
import br.com.tveiga.log.Level;
import br.com.tveiga.log.Log;
import br.com.tveiga.util.Directory;
import br.com.tveiga.util.FileUtil;
import br.com.tveiga.util.Normalize;

public class MainWindow extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static MainWindow self;
	private final String title = "Ambulatorio de Especialidades da Gastroenterologia";
	private JInternalFrameFactory frameFactory;
	private JInternalFrame internalFrame;
	private JFrameFerramentasFactory frameFerramentasFactory;
	private JFrame frameFerramentas;
	
	public static MainWindow getInstance(){
		return self ;
	}
	public MainWindow() throws ClassNotFoundException, InstantiationException,IllegalAccessException, UnsupportedLookAndFeelException {
		setTitle(title);
		setLocationRelativeTo(null);
		setResizable(true);
		setMinimumSize(new Dimension(400, 400));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		setJMenuBar(createMenuBar());
		JPanel footer = createFooter();
		add(footer, BorderLayout.SOUTH);
		pack();
		setExtendedState(MAXIMIZED_BOTH);
		frameFactory = JInternalFrameAbstractFactory.getJInternalFrameFactory(MenuType.Cadastro);
		menuItemEvent("paciente");
		self = this;
	}

	private JPanel createFooter() {
		JPanel footer = new JPanel(new GridLayout(1, 2));

		JPanel horario = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JPanel advertise = new JPanel(new FlowLayout(FlowLayout.RIGHT));

		JLabel dateTime = new JLabel();
		horario.add(dateTime);
		advertise.add(new JLabel("Desenvolvido por : TVeiga - Solucoes Tecnologicas em Saude"));

		footer.add(horario);
		footer.add(advertise);

		Thread threadTime = new Thread(new Runnable() {
			@Override
			public void run() {
				while (true) {
					String time = DateTime.now(DateTimeZone.getDefault()).toString(DateTimeFormat.fullDateTime());
					dateTime.setText(time);
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						Log.log(this.getClass().getName(), 91, e.getMessage(), Level.ERROR);
					}
				}

			}
		});
		threadTime.start();
		return footer;
	}

	private MouseAdapter getMouseListener(String itemName, String menu) {

		return new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				if("ferramentas".equals(menu))
					showFerramentasFrame(itemName);
				else
					menuItemEvent(itemName);
			}
		};
	}

	public void menuItemEvent(String itemName) {

		itemName = Normalize.normalizeName(itemName);
		switch (itemName) {
		case "topicosdeajuda":
			Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
			if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE))
				try {
					desktop.open(new File(Directory.HELP.getPath() + File.separator + "index.html"));
				} catch (IOException e) {
					Log.log(MainWindow.class.getSimpleName(), 120, e.getMessage(), Level.ERROR);
				}
			break;

		case "sobre":
			SobreFrame sobre = new SobreFrame();
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					sobre.setVisible(true);
				}
			});
			break;

		default:

			if (internalFrame != null) {
				internalFrame.dispose();
				internalFrame = null;
			}

			internalFrame = frameFactory.getJInternalFrame(itemName);
			if (internalFrame != null) {
				add(internalFrame);
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						internalFrame.setVisible(true);
					}
				});
			}
			
			break;
		}
	}

	private void showFerramentasFrame(String itemName){
		if(frameFerramentas != null){
			frameFerramentas.dispose();
			frameFerramentas = null;
		}
		
		frameFerramentas = 	frameFerramentasFactory.getJFrame(itemName);
		if(frameFerramentas != null){
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					frameFerramentas.setVisible(true);
				}
			});
		}
	}
	private void addMenuItens(JMenu jMenu, List<String> itens, String menu) {
		if (itens != null && !itens.isEmpty())
			for (String item : itens) {

				if (StringUtils.isBlank(item))
					continue;

				JMenuItem menuItem = new JMenuItem(item);
				menuItem.setName(item);
				menuItem.addMouseListener(getMouseListener(item, menu));
				jMenu.add(menuItem);
			}
	}

	private List<String> getItemList(String name) {
		String file = Directory.APP_SETTINGS.getPath() + File.separator + Normalize.normalizeName(name);
		return FileUtil.getItensFromFile(file);
	}

	private JMenuBar createMenuBar() {
		JMenuBar jMenuBar = new JMenuBar();

		for (String menu : getItemList("menu")) {

			if (StringUtils.isBlank(menu))
				continue;

			JMenu jMenu = new JMenu(menu, true);
			String menuName = Normalize.normalizeName(menu);
			jMenu.setName(menuName);

			if ("sair".equals(menuName)) {
				jMenu.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						int choise = JOptionPane.showOptionDialog(null, "Deseja realmente sair ?", "Sair", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, JOptionPane.NO_OPTION);
						if (JOptionPane.YES_OPTION == choise) {
							System.gc();
							System.exit(0);
						}
					}
				});
			} else
				buildMenu(menuName, jMenu);

			jMenuBar.add(jMenu);
		}

		return jMenuBar;
	}

	private void buildMenu(String menu, JMenu jMenu) {

		List<String> itens = getItemList(menu);
		addMenuItens(jMenu, itens, menu);

		if(!"ajuda".equals(menu)){
		  MenuType type = JInternalFrameAbstractFactory.getByName(menu);
		  jMenu.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				if("ferramentas".equals(menu))
					frameFerramentasFactory = JFrameFerramentasFactory.getInstance();
				else 
					frameFactory = JInternalFrameAbstractFactory.getJInternalFrameFactory(type);
			}
		  });
	    }
	}

}
