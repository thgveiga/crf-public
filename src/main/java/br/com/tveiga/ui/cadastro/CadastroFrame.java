package br.com.tveiga.ui.cadastro;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JToolBar;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;

import br.com.tveiga.bean.PersistentEntity;
import br.com.tveiga.bean.Usuario;
import br.com.tveiga.business.Business;
import br.com.tveiga.business.UsuarioBusiness;
import br.com.tveiga.dao.PersistentEntityDAO;
import br.com.tveiga.log.Level;
import br.com.tveiga.log.Log;
import br.com.tveiga.ui.main.MainWindow;
import br.com.tveiga.util.ButtonIcon;
import br.com.tveiga.util.Normalize;

public abstract class CadastroFrame extends JInternalFrame implements Printable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name;

	private JPanel jPanelToolBar;
	private JPanel jPanelFields;
	private Printable print;
	private static PersistentEntity persistentEntityCurrent;
	private List<JPanel> jpanelsDisabled;

	protected abstract JPanel createFieldsPanel();

	protected abstract PersistentEntity getObjectFromScreen();

	protected abstract Business<PersistentEntity, PersistentEntityDAO> getBusiness();

	protected void setPrint(Printable print) {
		this.print = print;
	}

	protected static PersistentEntity getPersistentEntityCurrent() {
		return persistentEntityCurrent;
	}

	protected static void setPersistentEntityCurrent(PersistentEntity persistentEntityCurrent) {
		CadastroFrame.persistentEntityCurrent = persistentEntityCurrent;
	}

	public CadastroFrame(String name) {
		super("Cadastro " + name);
		this.name = name;
		jpanelsDisabled = new ArrayList<JPanel>();
		jPanelToolBar = createToolBar();
		jPanelFields = createFieldsPanel();

		add(jPanelToolBar, BorderLayout.NORTH);
		add(jPanelFields, BorderLayout.CENTER);

	}

	@Override
	public int print(Graphics g, PageFormat pf, int page)
			throws PrinterException {

		Graphics2D g2 = (Graphics2D) g;
		if (page >= 1) {
			return Printable.NO_SUCH_PAGE;
		} else {
			AffineTransform originalTransform = g2.getTransform();

			double scaleX = pf.getImageableWidth() / jPanelFields.getWidth();
			double scaleY = pf.getImageableHeight() / jPanelFields.getHeight();
			double scale = Math.min(scaleX, scaleY);
			g2.translate(pf.getImageableX(), pf.getImageableY());
			g2.scale(scale, scale);
			jPanelFields.printAll(g2);

			g2.setTransform(originalTransform);

			return PAGE_EXISTS;
		}
	}

	protected String getNow() {
		return new DateTime().now(DateTimeZone.getDefault()).toString(
				DateTimeFormat.mediumDate());
	}

	protected void disableComponents(JPanel jPanel) {
		jpanelsDisabled.add(jPanel);
		Arrays.asList(jPanel.getComponents()).forEach(
				new Consumer<Component>() {
					@Override
					public void accept(Component t) {
						t.setEnabled(false);
					}
				});
	}

	protected void disableComponents() {
		jpanelsDisabled.forEach(new Consumer<JPanel>() {
			@Override
			public void accept(JPanel p) {
				Arrays.asList(p.getComponents()).forEach(
						new Consumer<Component>() {
							@Override
							public void accept(Component t) {
								t.setEnabled(false);
							}
						});
			}
		});
	}

	protected void enableComponents(boolean empty) {
		jpanelsDisabled.forEach(new Consumer<JPanel>() {
			@Override
			public void accept(JPanel p) {
				Arrays.asList(p.getComponents()).forEach(
						new Consumer<Component>() {
							@Override
							public void accept(Component t) {
								t.setEnabled(true);
								if (empty) {
									persistentEntityCurrent = null;
									if (t instanceof JTextField) {
										JTextField jTextField = (JTextField) t;
										jTextField.setText("");
									} else if (t instanceof JCheckBox) {
										JCheckBox jCheckBox = (JCheckBox) t;
										jCheckBox.setSelected(false);
									}
								}
							}
						});
			   }
		});
	}

	private JPanel createToolBar() {

		JToolBar toolBar = new JToolBar(JToolBar.HORIZONTAL);
		toolBar.setFloatable(false);
		toolBar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

		JPanel jPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		for (JButton button : getButtons())
			toolBar.add(button);

		jPanel.add(toolBar);

		return jPanel;

	}

	private List<JButton> getButtons() {

		List<JButton> buttons = new ArrayList<JButton>();

		Icon iconSalvar = new ImageIcon(ButtonIcon.BUTTON_SAVE.getPath());
		JButton buttonSalvar = new JButton(iconSalvar);

		Icon iconEditar = new ImageIcon(ButtonIcon.BUTTON_EDIT.getPath());
		JButton buttonEditar = new JButton(iconEditar);

		Icon iconNovo = new ImageIcon(ButtonIcon.BUTTON_NEW.getPath());
		JButton buttonNovo = new JButton(iconNovo);

		Icon iconExcluir = new ImageIcon(ButtonIcon.BUTTON_EXCLUIR.getPath());
		JButton buttonExcluir = new JButton(iconExcluir);
	
		Icon iconAnterior = new ImageIcon(ButtonIcon.BUTTON_PREVIOUS.getPath());
		JButton buttonAnterior = new JButton(iconAnterior);
	
		Icon iconImprimir = new ImageIcon(ButtonIcon.BUTTON_PRINT.getPath());
		JButton buttonImprimir = new JButton(iconImprimir);

		Icon iconProximo = new ImageIcon(ButtonIcon.BUTTON_NEXT.getPath());
		JButton buttonProximo = new JButton(iconProximo);

		Icon iconFechar = new ImageIcon(ButtonIcon.BUTTON_BACK.getPath());
		JButton buttonFechar = new JButton(iconFechar);
		
		buttonSalvar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (!buttonSalvar.isEnabled())
					return;
				PersistentEntity entity = getObjectFromScreen();
				if (entity == null)
					return;
				boolean ok = getBusiness().saveOrUpdate(persistentEntityCurrent, entity);
				if (ok) {
					JOptionPane.showMessageDialog(null, name + " salvo com sucesso ", null, JOptionPane.INFORMATION_MESSAGE);
					disableComponents();
					buttonNovo.setEnabled(true);
					buttonEditar.setEnabled(true);
					buttonExcluir.setEnabled(true);
					buttonAnterior.setEnabled(true);
					buttonProximo.setEnabled(true);
					buttonFechar.setEnabled(true);
					buttonSalvar.setEnabled(false);
				} else
					JOptionPane.showMessageDialog(null, " erro ao salvar " + StringUtils.lowerCase(name), null, JOptionPane.ERROR_MESSAGE);
			}
		});

		buttonSalvar.setFocusable(false);
		buttonSalvar.setEnabled(false);
		buttonExcluir.setFocusable(false);
				
		buttonNovo.setToolTipText("Novo(a) " + name);
		buttonEditar.setToolTipText("Editar " + name);
		buttonSalvar.setToolTipText("Salvar " + name);
		buttonExcluir.setToolTipText("Excluir " + name);
		
		buttons.add(buttonNovo);
		buttons.add(buttonEditar);
		buttons.add(buttonSalvar);
		buttons.add(buttonExcluir);
		
		buttonExcluir.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (!buttonExcluir.isEnabled())
					return;
				
				if ("usuario".equals(Normalize.normalizeName(name))) {
					Usuario usuario = (Usuario) persistentEntityCurrent;
					if (usuario.equals(UsuarioBusiness.getLoggedUser())){
						JOptionPane.showMessageDialog(null,	"nao foi possível excluir o usuario logado ",null, JOptionPane.ERROR_MESSAGE);
						return;
					}
				}
				
				int choise = JOptionPane.showOptionDialog(null, "Deseja realmente exucluir ?", "Excluir", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, JOptionPane.NO_OPTION);
				if (JOptionPane.YES_OPTION == choise) {
					boolean ok = getBusiness().delete(persistentEntityCurrent);
					if (ok)
						JOptionPane.showMessageDialog(null, name + "excluido com sucesso", null, JOptionPane.INFORMATION_MESSAGE);
					else
						JOptionPane.showMessageDialog(null, " erro ao excluir "	+ name, null, JOptionPane.ERROR_MESSAGE);
				}else
					return;
			}
		});

		buttonAnterior.setToolTipText(StringUtils.capitalize(name)
				+ " anterior");
		buttonAnterior.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (!buttonAnterior.isEnabled())
					return;

				persistentEntityCurrent = getBusiness().previous();
				refreshWindow(name);
			}
		});
		buttonAnterior.setFocusable(false);
		buttons.add(buttonAnterior);

		buttonProximo.setToolTipText("Proximo(a) " + name);
		buttonProximo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (!buttonProximo.isEnabled())
					return;

				persistentEntityCurrent = getBusiness().next();
				refreshWindow(name);
			}
		});
		buttonProximo.setFocusable(false);
		buttons.add(buttonProximo);

		buttonImprimir.setToolTipText("Imprimir " + name);
		buttonImprimir.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (!buttonImprimir.isEnabled())
					return;

				PrinterJob job = PrinterJob.getPrinterJob();
				job.setPrintable(print);
				if (job.printDialog()) {
					try {
						job.print();
					} catch (PrinterException ex) {
						Log.log(CadastroFrame.class.getSimpleName(), 220,
								ex.getMessage(), Level.ERROR);
					}
				}

			}
		});
		buttonImprimir.setFocusable(false);
		buttons.add(buttonImprimir);

		buttonFechar.setToolTipText("Voltar");
		buttonFechar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (!buttonFechar.isEnabled())
					return;

				dispose();
			}
		});
		buttonFechar.setFocusable(false);
		buttons.add(buttonFechar);

		buttonEditar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (!buttonEditar.isEnabled())
					return;

				if (persistentEntityCurrent == null)
					JOptionPane.showMessageDialog(null, "sem " + name + " para editar ", null, JOptionPane.ERROR_MESSAGE);
				else {
					buttonNovo.setEnabled(false);
					buttonSalvar.setEnabled(true);
					buttonFechar.setEnabled(true);
					buttonEditar.setEnabled(false);
					buttonExcluir.setEnabled(false);
					buttonImprimir.setEnabled(false);
					buttonAnterior.setEnabled(false);
					buttonProximo.setEnabled(false);
					enableComponents(false);
				}
			}
		});
		buttonEditar.setFocusable(false);

		buttonNovo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (!buttonNovo.isEnabled())
					return;

				persistentEntityCurrent = null;
				buttonNovo.setEnabled(false);
				buttonSalvar.setEnabled(true);
				buttonFechar.setEnabled(true);
				buttonEditar.setEnabled(false);
				buttonExcluir.setEnabled(false);
				buttonImprimir.setEnabled(false);
				buttonAnterior.setEnabled(false);
				buttonProximo.setEnabled(false);
				enableComponents(true);
			}
		});
		buttonNovo.setFocusable(false);

		return buttons;
	}

	private void refreshWindow(String name) {
		dispose();
		MainWindow.getInstance().menuItemEvent(name);
	}
}
