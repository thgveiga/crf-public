package br.com.tveiga.ui.cadastro;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFormattedTextField.AbstractFormatter;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import net.sourceforge.jdatepicker.impl.JDatePanelImpl;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;
import net.sourceforge.jdatepicker.impl.UtilDateModel;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import br.com.tveiga.bean.Paciente;
import br.com.tveiga.util.ButtonIcon;

public abstract class CadastroTab {

	private int itemCount;
	
	protected abstract JPanel createPanel() throws ParseException;
	public abstract Paciente getPacienteDataFromScreen();
	
	protected JDatePickerImpl createDatePicker(){
		UtilDateModel model = new UtilDateModel();
	    JDatePanelImpl datePanel = new JDatePanelImpl(model);
	    
	    AbstractFormatter formatter = new AbstractFormatter() {
	    	
			public String valueToString(Object value) throws ParseException {
				String datePattern =  "";
				
				if(value != null){
					Calendar calendar = (Calendar) value;
					DateTime dateTime = new DateTime(value, DateTimeZone.getDefault());
					datePattern = dateTime.toString(DateTimeFormat.mediumDate());
				}
				return datePattern;
			}
			
			public Object stringToValue(String text) throws ParseException {
				DateTimeFormatter formatter = DateTimeFormat.mediumDate();
				DateTime dateTime = formatter.parseDateTime(text);
				return dateTime;
			}
		};

	    JDatePickerImpl datePicker = new JDatePickerImpl(datePanel, formatter);
	    return datePicker;
	}
	
	
	protected String calculaIdade(Object value){
		String idadeStr = "";
		if(value != null){
			Date date = (Date) value;
			DateTime dateTime = new DateTime(date);
			DateTime now = DateTime.now();
			if(dateTime.isBeforeNow()){
				Period period = new Period(dateTime, now);
				idadeStr =  Integer.toString(period.getYears());
			}else{
				JOptionPane.showMessageDialog(null, "A data de nascimento e maior que a data atual ", null, JOptionPane.ERROR_MESSAGE);
			}
		}
		return idadeStr;
	}
	
	protected String calculaPeriodo(Date dateInicio, Date dateFim){
		String periodoStr = "";
		DateTime dateTimeInicio = new DateTime(dateInicio);
		DateTime dateTimeFim = new DateTime(dateFim);
		Period period = new Period(dateTimeInicio, dateTimeFim, PeriodType.yearMonthDayTime());
		String anos, meses, dias = "";
		anos = period.getYears() < 0 ? period.getYears() + " ano(s) " : "";
		meses = period.getMonths() < 0 ? period.getMonths() + " mes(es) " : "";
		anos = period.getDays() < 0 ? period.getDays() + " dia(s) " : "";
		periodoStr =  anos + meses + dias;
		return periodoStr;
	}
	
	protected String getNow(){
		return new DateTime().now(DateTimeZone.getDefault()).toString(DateTimeFormat.mediumDate());
	}
	
	protected boolean adicionaRegistro(JPanel list, JScrollPane scrollPane, int width, int height , String... args) {

		width = width - 10;
		scrollPane.setPreferredSize(new Dimension(width, height));
		
		//JPanel jPanelRegistros = new JPanel(new GridLayout(1,args.length + 2));
		JPanel jPanelRegistros = new JPanel();
		JTextField fieldNumber = new JTextField(String.valueOf(++itemCount));
		fieldNumber.setEditable(false);
		fieldNumber.setName("number");
		jPanelRegistros.add(fieldNumber);
		
		JButton removeButton = new JButton(new ImageIcon(ButtonIcon.BUTTON_CANCEL.getPath()));
		removeButton.setText("Remover Registro");
		removeButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				list.remove(jPanelRegistros);
				itemCount = 0; 
				for(Component c : list.getComponents()){
					if(c instanceof JPanel){
						JPanel p = (JPanel)c;
						for(Component cp : p.getComponents()){
							if("number".equals(cp.getName())){
								JTextField t = (JTextField)cp;
								t.setText(String.valueOf(++itemCount));
							}
						}
					}
				}
			}
		});
		
		for(String value :args){
			
			if(StringUtils.isBlank(value)){
				JOptionPane.showMessageDialog(null, "Todos os campos devem ser preenchidos ", null, JOptionPane.ERROR_MESSAGE);
				--itemCount;
				return false;
			}
			
			String temp = value.length() <= 45 ? value : value.substring(0, 45).concat(" ...");
			JTextField field = new JTextField(temp);
			field.setToolTipText(value);
			field.setEditable(false);
			jPanelRegistros.add(field);
			jPanelRegistros.add(removeButton);
		}
		
		return list.add(jPanelRegistros) != null;
	}
	
	

}
