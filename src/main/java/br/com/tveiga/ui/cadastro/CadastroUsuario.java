package br.com.tveiga.ui.cadastro;

import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import br.com.tveiga.bean.Permissao;
import br.com.tveiga.bean.PersistentEntity;
import br.com.tveiga.bean.Usuario;
import br.com.tveiga.business.Business;
import br.com.tveiga.business.UsuarioBusiness;
import br.com.tveiga.util.Directory;
import br.com.tveiga.util.FileUtil;
import br.com.tveiga.util.Normalize;


public class CadastroUsuario extends CadastroFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static Usuario usuarioCurrent;
	
	//campos com dados do objeto persistente
	private JTextField jTextFieldNome;
	private JPasswordField jPasswordFieldSenha;
	private JPasswordField jPasswordFieldConfirmaSenha;
	private JTextField jTextFieldCreated;
	private JTextField jTextFieldUpdated;
	private List<Permissao> permissoesScreen;
	private UsuarioBusiness usuarioBusiness;
	
	public CadastroUsuario() {
		super("Usuário");
	}
	
	private void createTitle(String title, JPanel jPanel){
	
		JPanel jPanelTitulo = new JPanel(new FlowLayout(FlowLayout.CENTER));
		jPanelTitulo.setName("titulo");
		String titulo = title + " do Usuario ";
		
		jPanelTitulo.add(new JLabel(titulo));

		jPanel.add(new JLabel());
		jPanel.add(jPanelTitulo);
		jPanel.add(new JLabel());

	}
	
	private final JPanel createUserPanel(){
		JPanel jPanelUsuario = new JPanel(new GridLayout(16,1));
		
		createTitle("Dados", jPanelUsuario);

		String nome = usuarioCurrent != null ? usuarioCurrent.getNome() : "";
		String now = getNow();
		String created = usuarioCurrent != null ? usuarioCurrent.getCreatedAt().toString(DateTimeFormat.mediumDate()) : now;
		String updated = usuarioCurrent != null ? usuarioCurrent.getCreatedAt().toString(DateTimeFormat.mediumDate()) : now;
		
		JPanel jPanelNome = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel jLabelNome = new JLabel("Nome do usuario:    ");
		
		jTextFieldNome = new JTextField(50);
		jTextFieldNome.setText(nome);
		
		jTextFieldNome.setFocusable(true);
		jLabelNome.setLabelFor(jTextFieldNome);
		jPanelNome.add(jLabelNome);
		jPanelNome.add(jTextFieldNome);
		
		JPanel jPanelSenha = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel jLabelSenha = new JLabel("Senha:                    ");
		jPasswordFieldSenha = new JPasswordField(30);
		jLabelSenha.setLabelFor(jPasswordFieldSenha);
		
		jPanelSenha.add(jLabelSenha);
		jPanelSenha.add(jPasswordFieldSenha);
		
		JPanel jPanelConfirmaSenha = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel jLabelConfirmaSenha = new JLabel("Confirmar senha:    ");
		jPasswordFieldConfirmaSenha = new JPasswordField(30);
		jLabelConfirmaSenha.setLabelFor(jPasswordFieldConfirmaSenha);
		jPanelConfirmaSenha.add(jLabelConfirmaSenha);
		jPanelConfirmaSenha.add(jPasswordFieldConfirmaSenha);

		JPanel jPanelCreated = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel jLabelCreated = new JLabel("Criado em:              ");
		
		jTextFieldCreated = new JTextField(30);
		jTextFieldCreated.setText(created);
		jTextFieldCreated.setEditable(false);
		jLabelCreated.setLabelFor(jTextFieldCreated);
		jPanelCreated.add(jLabelCreated);
		jPanelCreated.add(jTextFieldCreated);

		JPanel jPanelUpdated = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel jLabelUpdated = new JLabel("Atualizado em:       ");
		
		jTextFieldUpdated = new JTextField(30);
		jTextFieldUpdated.setText(updated);
		jTextFieldUpdated.setEditable(false);
		jLabelUpdated.setLabelFor(jTextFieldUpdated);
		jPanelUpdated.add(jLabelUpdated);
		jPanelUpdated.add(jTextFieldUpdated);
		
		jPanelUsuario.add(jPanelNome);
		jPanelUsuario.add(jPanelSenha);
		jPanelUsuario.add(jPanelConfirmaSenha);
		jPanelUsuario.add(jPanelCreated);
		jPanelUsuario.add(jPanelUpdated);

		Arrays.asList(jPanelUsuario.getComponents()).forEach(new Consumer<Component>() {
			@Override
			public void accept(Component t) {
				if(t instanceof JPanel){
					JPanel jPanel = (JPanel)t;
					if(!"titulo".equals(jPanel.getName()))
						disableComponents(jPanel);
				}
			}});
			
		return jPanelUsuario;
		
	}
	
	
	private Component createPermissionPanel() {
		
		JPanel jPanelPermissoes = new JPanel(new GridLayout(20,1));

		createTitle("Permissoes", jPanelPermissoes);
		
		String file = Directory.APP_SETTINGS.getPath() + File.separator + Normalize.normalizeName("pesquisa");
		List<String> itens = FileUtil.getItensFromFile(file, false);
		
		List<Permissao> permissoes = usuarioCurrent.getPermissoes();
		for(String item : itens){
			
			JPanel jPanelItem = new JPanel(new GridLayout(1,2));
			
			Permissao permissao = null;	

			if(permissoes != null & !permissoes.isEmpty())
				for(Permissao p : permissoes)
					if(p.getNome().equalsIgnoreCase(Normalize.normalizeName(item))){
						permissao = p;
						break;
					}
			
			JLabel jLabelItem = new JLabel(item + " :");                      

			Permissao permissaoScreen =  new Permissao();
			permissaoScreen.setNome(item);
			permissaoScreen.setCreatedAt(new DateTime().now());
			permissaoScreen.setUpdatedAt(new DateTime().now());
			
			JCheckBox checkBoxVerItem = new JCheckBox("visualizar");
			checkBoxVerItem.setSelected(permissao != null && permissao.isView());
			permissaoScreen.setView(checkBoxVerItem.isSelected());
			
			JCheckBox checkBoxNovoItem = new JCheckBox("criar");
			checkBoxNovoItem.setSelected(permissao != null && permissao.isCreate());
			permissaoScreen.setCreate(checkBoxNovoItem.isSelected());
			
			JCheckBox checkBoxEditarItem = new JCheckBox("editar");
			checkBoxEditarItem.setSelected(permissao != null && permissao.isEdit());
			permissaoScreen.setEdit(checkBoxEditarItem.isSelected());
			
			JCheckBox checkBoxExcluirItem = new JCheckBox("excluir");
			checkBoxExcluirItem.setSelected(permissao != null && permissao.isRemove());
			permissaoScreen.setEdit(checkBoxExcluirItem.isSelected());
			
			jPanelItem.add(jLabelItem);
			jPanelItem.add(checkBoxVerItem);
			jPanelItem.add(checkBoxNovoItem);
			jPanelItem.add(checkBoxEditarItem);
			jPanelItem.add(checkBoxExcluirItem);

			if("permissao".equalsIgnoreCase(Normalize.normalizeName(item))){
				checkBoxNovoItem.setVisible(false);
				checkBoxExcluirItem.setVisible(false);
			}
		
			disableComponents(jPanelItem);
			permissoesScreen.add(permissaoScreen);
			jPanelPermissoes.add(jPanelItem);
		}
		
		return jPanelPermissoes;
	}
	
	@Override
	protected JPanel createFieldsPanel() {
		setPrint(this);
		permissoesScreen = new ArrayList<Permissao>();
		usuarioCurrent = getPersistentEntityCurrent() == null ? UsuarioBusiness.getLoggedUser() : (Usuario)getPersistentEntityCurrent();
		setPersistentEntityCurrent(usuarioCurrent);
		usuarioBusiness = new UsuarioBusiness(usuarioCurrent);
		
		JPanel jPanel = new JPanel(new GridLayout(1,2));
		jPanel.add(createUserPanel());
		jPanel.add(createPermissionPanel());
		return jPanel;
		
	}

	@Override
	protected PersistentEntity getObjectFromScreen() {
		Usuario usuario = new Usuario();
		
		String nome = jTextFieldNome.getText();
		String created_at = jTextFieldCreated.getText();
		String senha = new String(jPasswordFieldSenha.getPassword());
		String confirmaSenha = new String(jPasswordFieldConfirmaSenha.getPassword());
		
		usuario.setNome(nome);
		usuario.setCreatedAt(created_at != null ? usuario.getCreatedAt() : new DateTime());
		usuario.setUpdatedAt(new DateTime());
		
		if(StringUtils.isNotBlank(senha) && StringUtils.isNotBlank(confirmaSenha) && senha.equals(confirmaSenha))
			usuario.setSenha(senha);
		else{
			JOptionPane.showMessageDialog(null, " a senha e a confirmacao de senha devem ser iguais" , null, JOptionPane.ERROR_MESSAGE);
			return null;
		}
			
		
		usuario.setPermissoes(permissoesScreen);
		usuario.setActive(true);
		
		return usuario;
	}

	@Override
	protected Business getBusiness() {
		return usuarioBusiness;
	}

}
