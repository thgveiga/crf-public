package br.com.tveiga.ui.cadastro;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.function.Consumer;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import net.sourceforge.jdatepicker.impl.JDatePickerImpl;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import br.com.tveiga.bean.Paciente;
import br.com.tveiga.bean.Patologia;
import br.com.tveiga.business.PatologiaBussiness;
import br.com.tveiga.util.ButtonIcon;

public class CadastroHistorico extends CadastroTab {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JCheckBox checkBoxNaoRealizado;
	private JCheckBox checkBoxCirurgicaRelevante;
	private JCheckBox checkBoxCirurgiaProgramada;
	
	private JDatePickerImpl datePickerDataDeInicio;
	private JDatePickerImpl datePickerDataDeTermino;
	private JCheckBox checkBoxContinua;
	private JTextField jTextFieldCID;
	private JPanel panelPatologiasList;
	private int itemCount;
	private List<Patologia> patologias;
	private JTextField jTextFieldDiagnostico;
	private JFrame frameSelection;
	private int width;
	private JScrollPane scroll;
	private JPanel jPanelCenter;
	private Paciente paciente;
	
	private PatologiaBussiness patologiaBussiness;

	public CadastroHistorico() {
	}
	
	public CadastroHistorico(Paciente paciente) {
		this.paciente = paciente;	
	}

	@Override
	protected JPanel createPanel() {
		
		patologiaBussiness = new PatologiaBussiness();
		JPanel jPanelGeral  = new JPanel(new BorderLayout());
		jPanelCenter = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JPanel jPanelSouth = new JPanel();
		
		JPanel panelHistoria = new JPanel(new FlowLayout(FlowLayout.CENTER));
		panelHistoria.add(new JLabel("Ambulatorio de Doencas Inflamatorias Intestinais - FMABC"));

		JPanel panelNaoRealizado = new JPanel(new FlowLayout(FlowLayout.LEFT));
		checkBoxNaoRealizado = new JCheckBox("Nao Realizado");
		panelNaoRealizado.add(checkBoxNaoRealizado);
		
		JPanel panelCirurgicaRelevante = new JPanel(new FlowLayout(FlowLayout.LEFT));
		checkBoxCirurgicaRelevante = new JCheckBox("O paciente nao apresenta Historia Clinica e Cirurgica Relevante");
		panelCirurgicaRelevante.add(checkBoxCirurgicaRelevante);
		
		JPanel panelCirurgiaProgramada = new JPanel(new FlowLayout(FlowLayout.LEFT));
		checkBoxCirurgiaProgramada = new JCheckBox("O paciente nao apresenta Cirurgia Programada");
		panelCirurgiaProgramada.add(checkBoxCirurgiaProgramada);
		
		JPanel panelDescrever = new JPanel(new FlowLayout(FlowLayout.LEFT));
		panelDescrever.add(new JLabel("Descrever apenas uma condicao por linha"));

		JPanel linhas = new JPanel(new GridLayout(5,1));
		linhas.add(panelNaoRealizado);
		linhas.add(panelCirurgicaRelevante);
		linhas.add(panelCirurgiaProgramada);
		linhas.add(panelDescrever);
		
		JPanel panelPatologias = new JPanel(new GridLayout(2,0));
		panelPatologias.setToolTipText("Para cirurgias, reporte a data de termino identica a data de inicio.");

		panelPatologias.add(new JLabel("Diagnostico ou Sintoma"));
		panelPatologias.add(new JLabel("Data de início"));
		panelPatologias.add(new JLabel("Data de Termino"));
		panelPatologias.add(new JLabel("Continua ?"));
		panelPatologias.add(new JLabel("CID"));
		panelPatologias.add(new JLabel());
		
		patologias = patologiaBussiness.getAll();
		jTextFieldDiagnostico = new JTextField(25);		
		
		datePickerDataDeInicio = createDatePicker();
		datePickerDataDeTermino = createDatePicker();
		jTextFieldCID = new JTextField(10);
		jTextFieldCID.setEditable(false);
		checkBoxContinua = new JCheckBox("Nao Continua");
		checkBoxContinua.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
			  checkBoxContinua.setText(checkBoxContinua.isSelected() ? "Continua" : "Nao Continua");
			}
		});

		//GridLayout gridLayout = new GridLayout(0, 1, 0, 0);
		panelPatologiasList = new JPanel();
		BoxLayout boxLayout = new BoxLayout(panelPatologiasList, BoxLayout.Y_AXIS);
		panelPatologiasList.setLayout(boxLayout);
		
		JButton jButtonAdd = new JButton(new ImageIcon(ButtonIcon.BUTTON_ADD.getPath()));
		jButtonAdd.setText("Adicionar Registro");
		
		panelPatologias.add(jTextFieldDiagnostico);
		panelPatologias.add(datePickerDataDeInicio);
		panelPatologias.add(datePickerDataDeTermino);
		panelPatologias.add(checkBoxContinua);
		panelPatologias.add(jTextFieldCID);
		panelPatologias.add(jButtonAdd);
		
		jPanelCenter.add(linhas);
		jPanelCenter.add(panelPatologias);
		
		scroll = new JScrollPane(panelPatologiasList);
		
		scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		
		jPanelCenter.add(scroll);
		
		jTextFieldDiagnostico.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				new Thread(new Runnable() {
					
					@Override
					public void run() {
						JList<Patologia> texto = new JList<Patologia>();
						texto.addListSelectionListener(new ListSelectionListener() {
							@Override
							public void valueChanged(ListSelectionEvent e) {
								Patologia patologia = texto.getSelectedValue();
								jTextFieldDiagnostico.setText(patologia.getNome());
								jTextFieldCID.setText(patologia.getCid());
								jTextFieldDiagnostico.transferFocus();
								frameSelection.dispose();
								frameSelection = null;
							}
						});
						
						List<Patologia> list = new ArrayList<Patologia>();
						patologias.forEach(new Consumer<Patologia>() {
						   @Override
     						public void accept(Patologia patologia) {
							  if(patologia.getNome().toLowerCase().startsWith(jTextFieldDiagnostico.getText().toLowerCase()))
								  list.add(patologia);
							}
	    				});
						   
					 texto.setListData(list.toArray(new Patologia[list.size()]));  

					 if(frameSelection != null){
						frameSelection.dispose();
						frameSelection = null;
					 }
						 
  	                 frameSelection = new JFrame();
				     frameSelection.setResizable(false);
				     JScrollPane scrollpane = new JScrollPane(texto);
				     Container pane = frameSelection.getContentPane(); 
    			     pane.setLayout(new FlowLayout(FlowLayout.LEFT));
				     scrollpane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
				     scrollpane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
				     pane.add(scrollpane);
				     frameSelection.pack();
				     frameSelection.setAlwaysOnTop(true);
				     Point point = jTextFieldDiagnostico.getLocationOnScreen();
				     frameSelection.setLocation((int)point.getX(), (int)(point.getY() + jTextFieldDiagnostico.getHeight()));
				     frameSelection.setVisible(true);
				     jTextFieldDiagnostico.setFocusable(true);
				     jTextFieldDiagnostico.requestFocus();
				     
				  }
				}).start();
			}
		});
		
		jButtonAdd.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String diagnostico = jTextFieldDiagnostico.getText();
				Date dataInicio = (Date)datePickerDataDeInicio.getModel().getValue();
				Date dataTermino = (Date)datePickerDataDeTermino.getModel().getValue();
				String continua = checkBoxContinua.getText();
				String cid = jTextFieldCID.getText();
				
				boolean sucess = adicionaRegistro(panelPatologiasList, scroll, jPanelCenter.getSize().width, 210, diagnostico, (dataInicio != null ? new DateTime(dataInicio).toString(DateTimeFormat.mediumDate()) : getNow()), (dataTermino != null ? new DateTime(dataTermino).toString(DateTimeFormat.mediumDate()) : getNow()), continua, cid);
				
				if(sucess){
					jTextFieldDiagnostico.setText("");
					datePickerDataDeInicio.getModel().setValue(null);
					datePickerDataDeTermino.getModel().setValue(null);
					checkBoxContinua.setSelected(false);
					checkBoxContinua.setText("Nao Continua");
					jTextFieldCID.setText("");
					jTextFieldDiagnostico.requestFocus();
				}
			}
		});
		
		JPanel jPanelNorth = new JPanel(new FlowLayout(FlowLayout.CENTER));

		jPanelNorth.add(panelHistoria);
		jPanelGeral.add(jPanelNorth, BorderLayout.NORTH);
		jPanelGeral.add(jPanelCenter, BorderLayout.CENTER);
		jPanelGeral.add(jPanelSouth, BorderLayout.SOUTH);
		
		return jPanelGeral;
	}

	@Override
	public Paciente getPacienteDataFromScreen() {
		return paciente;
	}

}
