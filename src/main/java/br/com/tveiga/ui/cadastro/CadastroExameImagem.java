package br.com.tveiga.ui.cadastro;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import net.sourceforge.jdatepicker.impl.JDatePickerImpl;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import br.com.tveiga.bean.ExameImagem;
import br.com.tveiga.bean.Paciente;
import br.com.tveiga.util.ButtonIcon;


public class CadastroExameImagem  extends CadastroTab {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel titulo;
	private JScrollPane scroll;
	private JPanel lista;
	private Paciente paciente;
	
	private JRadioButton radioButtonNormal;
	private JRadioButton radioButtonAnormal;
	private JDatePickerImpl dataPicker;
	private JTextArea textAreaDescricao;
	private List<ExameImagem> examesDeImagem = new ArrayList<ExameImagem>();
	
	public CadastroExameImagem(Paciente paciente) {
		this.paciente = paciente;
	}

	@Override
	protected JPanel createPanel() {
		JPanel jPanel = new JPanel();
		titulo = new JPanel(new GridLayout(0,1));
		titulo.add(new JLabel("COLONOSCOPIA e EDA - DESCREVA APENAS OS DADOS POSITIVOS. Mencione se o ileo foi examinado", JLabel.CENTER));
		
		JPanel descricao = new JPanel(new FlowLayout(FlowLayout.LEFT));
		
		JPanel dados = new JPanel(new GridLayout(0,1));
		
		JPanel normal = new JPanel(new FlowLayout(FlowLayout.LEFT));
		radioButtonNormal = new JRadioButton("Normal");
		radioButtonNormal.setSelected(true);
		radioButtonAnormal = new JRadioButton("Anormal");
	    ButtonGroup  buttonGroupNormal = new ButtonGroup();
	    buttonGroupNormal.add(radioButtonNormal);
	    buttonGroupNormal.add(radioButtonAnormal);
	    
	    JPanel data = new JPanel(new FlowLayout(FlowLayout.LEFT));
	    JLabel dataLabel = new JLabel("Data:");
	    dataPicker = createDatePicker();
	    dataLabel.setLabelFor(dataPicker);
	    
	    data.add(dataLabel);
	    data.add(dataPicker);
	    
	    normal.add(radioButtonNormal);
	    normal.add(radioButtonAnormal);

	    JPanel buttonPanel = new JPanel();
		JButton jButtonAdd = new JButton(new ImageIcon(ButtonIcon.BUTTON_ADD.getPath()));
		jButtonAdd.setText("Adicionar Registro");
		buttonPanel.add(jButtonAdd);
		
	    dados.add(data);
	    dados.add(normal);
	    
		textAreaDescricao = new JTextArea(2,100);
		textAreaDescricao.setText("Descricao Sucinta ...");
		textAreaDescricao.setLineWrap(true);
		textAreaDescricao.addFocusListener(new FocusListener() {
			
			@Override
			public void focusLost(FocusEvent e) {
				if(StringUtils.isBlank(textAreaDescricao.getText())){
					textAreaDescricao.setText("Descricao Sucinta ...");
				}
			}
			
			@Override
			public void focusGained(FocusEvent e) {
				if("Descricao Sucinta ...".equals(textAreaDescricao.getText())){
					textAreaDescricao.setText("");
				}
			}
		});
		
		JScrollPane textAreaBar = new JScrollPane(textAreaDescricao);
		textAreaBar.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		textAreaBar.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		
		descricao.add(dados);
		descricao.add(textAreaBar);
		descricao.add(buttonPanel);
		
		titulo.add(descricao);
		
		lista = new JPanel();
		
		BoxLayout boxLayout = new BoxLayout(lista, BoxLayout.Y_AXIS);
		lista.setLayout(boxLayout);
		scroll = new JScrollPane(lista);
		
		scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		
		jPanel.add(titulo, BorderLayout.NORTH);
		jPanel.add(scroll, BorderLayout.CENTER);
		
		jButtonAdd.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Date inicio = dataPicker.getModel().getValue() == null ? new Date() : (Date) dataPicker.getModel().getValue();
				String dataInicio =  inicio == null ? getNow() : new DateTime(inicio).toString(DateTimeFormat.mediumDate());
				String descricao = textAreaDescricao.getText();
				String normal = radioButtonNormal.isSelected() ? radioButtonNormal.getText() : radioButtonAnormal.getText();
				
				boolean sucess = adicionaRegistro(lista, scroll, titulo.getSize().width, 300, dataInicio, descricao, normal);
				if(sucess){
					
					ExameImagem exameImagem = new ExameImagem();
					exameImagem.setActive(true);
					exameImagem.setUpdatedAt(new DateTime());
					exameImagem.setCreatedAt(new DateTime());
					exameImagem.setNome("COLONOSCOPIA e EDA");
					exameImagem.setDescricao(descricao);
					exameImagem.setNormal(normal);
					exameImagem.setInicio(new DateTime(inicio));
					
					examesDeImagem.add(exameImagem);
					
					dataPicker.getModel().setValue(null);
					textAreaDescricao.setText("Descricao Sucinta ...");
					radioButtonNormal.setSelected(true);
				}
			}
		});
		
		return jPanel;
	}

	@Override
	public Paciente getPacienteDataFromScreen() {
		return paciente;
	}
}
