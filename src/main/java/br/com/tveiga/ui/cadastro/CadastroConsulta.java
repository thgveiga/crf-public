package br.com.tveiga.ui.cadastro;

import java.awt.BorderLayout;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import br.com.tveiga.bean.Consulta;
import br.com.tveiga.bean.Paciente;


public class CadastroConsulta extends CadastroTab {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Paciente paciente;
	
	public CadastroConsulta(Paciente paciente) {
		this.paciente = paciente;
	}

	@Override
	protected JPanel createPanel() {
		JPanel jPanel = new JPanel();
		
		JScrollPane scroll = new JScrollPane();
		scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		
		Consulta[] cosultas = new Consulta[1];
		JList<Consulta> jListConsulta = new JList<Consulta>(cosultas);
		
		JLabel jLabel = new JLabel("Consultas : ");
		
		scroll.add(jListConsulta);
		
		jPanel.add(jLabel, BorderLayout.NORTH);
		jPanel.add(scroll, BorderLayout.CENTER);
		
		return jPanel;
	}

	@Override
	public Paciente getPacienteDataFromScreen() {
		return paciente;
	}
}
