package br.com.tveiga.ui.cadastro;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.text.ParseException;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.text.MaskFormatter;

import net.sourceforge.jdatepicker.impl.JDatePickerImpl;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;

import br.com.tveiga.bean.Paciente;
import br.com.tveiga.util.Directory;
import br.com.tveiga.util.FileUtil;
import br.com.tveiga.util.Normalize;

public class CadastroDadosDemograficos extends CadastroTab {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JTextField textFieldNome;
	private JTextField textFieldSAME;
	private JDatePickerImpl datePickerDataDeAdmissao;
	private JRadioButton  radioButtonMasculino;
	private JRadioButton  radioButtonFeminino;
	private JDatePickerImpl datePickerDataDeNascimento;
	private JTextField textFieldIdade;
	private JRadioButton  radioButtonCasado;
	private JRadioButton  radioButtonDivorciado;
    private JRadioButton  radioButtonSolteiro;
    private JRadioButton  radioButtonViuvo;
    
    private JRadioButton  radioButtonAmarela;
    private JRadioButton  radioButtonBranca;
    private JRadioButton  radioButtonIndigena;
    private JRadioButton  radioButtonNegra;
    private JRadioButton  radioButtonParda;
    
    private JTextField textFieldRua;
    private JTextField textFieldBairro;
    private JComboBox<String> jComboEstados;
    private JTextField textFieldCEP;

    private JTextField textFieldResidencial;
    private JTextField textFieldComercial;
    private JTextField textFieldCelular;
    private JTextField textFieldRecados;
    private JTextField textFieldProcedente;
    private JTextField textFieldHa;
    private JComboBox<String> JComboHa;
    private JTextField textFieldProfissao;
    private JComboBox<String> jComboEscolaridade;
    private JComboBox<String> jComboCompleto;
    private JRadioButton radioButtonConvenioSim;
    private JRadioButton radioButtonConvenioNao;
    private Paciente paciente;
    
	public CadastroDadosDemograficos(Paciente paciente) {
		this.paciente = paciente;
	}

	@Override
	protected JPanel createPanel() throws ParseException {
		
		JPanel panelDadosDemograficos = new JPanel(new GridLayout(12, 1));
		JPanel panelDadosDemograficos2 = new JPanel(new GridLayout(12, 1));
		
		JPanel panelTitulo = new JPanel(new FlowLayout(FlowLayout.CENTER));
	    JLabel labelTitilo = new JLabel("Ambulatorio de Doenças Inflamatorias Intestinais - FMABC");
	    panelTitulo.add(labelTitilo);
	    
	    JPanel panelNome = new JPanel(new FlowLayout(FlowLayout.LEFT));
	    JLabel labelNome = new JLabel("Nome:                         ");
	    textFieldNome = new JTextField(60);
	    textFieldNome.setText(paciente != null ? paciente.getNome() : "");
	    
	    labelNome.setLabelFor(textFieldNome);
	    panelNome.add(labelNome);
	    panelNome.add(textFieldNome);
	    
	    JPanel panelSAME = new JPanel(new FlowLayout(FlowLayout.LEFT));
	    JLabel labelSAME = new JLabel("Prontuario (nº SAME):");
	    textFieldSAME = new JTextField(50);
	    textFieldSAME.setText(paciente != null ? paciente.getSame() : "");
	    
	    labelSAME.setLabelFor(textFieldSAME);
	    panelSAME.add(labelSAME);
	    panelSAME.add(textFieldSAME);

	    JPanel panelDataDeAdmissao = new JPanel(new FlowLayout(FlowLayout.LEFT));
	    datePickerDataDeAdmissao = createDatePicker();
	    datePickerDataDeAdmissao.setEnabled(true);
	    
	    if(paciente != null && paciente.getDataDeAdmissao() != null){
	    	datePickerDataDeAdmissao.getModel().setDay(paciente.getDataDeAdmissao().getDayOfMonth());
	    	datePickerDataDeAdmissao.getModel().setMonth(paciente.getDataDeAdmissao().getMonthOfYear());
	    	datePickerDataDeAdmissao.getModel().setYear(paciente.getDataDeAdmissao().getYear());
	    }
	    
	    JLabel labelDataDeAdmissao = new JLabel("Data de admissao:     ");
	    labelDataDeAdmissao.setLabelFor(datePickerDataDeAdmissao);
	    panelDataDeAdmissao.add(labelDataDeAdmissao);
	    panelDataDeAdmissao.add(datePickerDataDeAdmissao);
	    
	    JPanel panelSexo = new JPanel(new FlowLayout(FlowLayout.LEFT));
	    JLabel labelSexo = new JLabel("Sexo:                         ");
	    radioButtonMasculino = new JRadioButton("Masculino");
	    radioButtonFeminino = new JRadioButton("Feminino");
	    radioButtonMasculino.setSelected(paciente != null && paciente.getSexo().equalsIgnoreCase("Masculino"));
	    radioButtonFeminino.setSelected(paciente != null && paciente.getSexo().equalsIgnoreCase("Feminino"));
	    
	    ButtonGroup  buttonGroupSexo = new ButtonGroup();
	    buttonGroupSexo.add(radioButtonMasculino);
	    buttonGroupSexo.add(radioButtonFeminino);
	    
	    panelSexo.add(labelSexo);
	    panelSexo.add(radioButtonMasculino);
	    panelSexo.add(radioButtonFeminino);
	    
	    JPanel panelDataDeNascimento = new JPanel(new FlowLayout(FlowLayout.LEFT));
	    datePickerDataDeNascimento = createDatePicker();
	    
	    datePickerDataDeNascimento.setEnabled(true);
	    
	    if(paciente != null && paciente.getDataDeAdmissao() != null){
	    	datePickerDataDeNascimento.getModel().setDay(paciente.getDataDeNascimento().getDayOfMonth());
	    	datePickerDataDeNascimento.getModel().setMonth(paciente.getDataDeNascimento().getMonthOfYear());
	    	datePickerDataDeNascimento.getModel().setYear(paciente.getDataDeNascimento().getYear());
	    }
	    
	    JLabel labelDataDeNascimento = new JLabel("Data de nascimento: ");
	    labelDataDeNascimento.setLabelFor(datePickerDataDeNascimento);
	    panelDataDeNascimento.add(labelDataDeNascimento);
	    panelDataDeNascimento.add(datePickerDataDeNascimento);
	    JLabel labelIdade = new JLabel("Idade:");
	    textFieldIdade = new JTextField(10);
	    textFieldIdade.setEditable(false);
	    textFieldIdade.setText(paciente != null ? paciente.getIdade().toString() : "");
	    
	    datePickerDataDeNascimento.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Thread thread = new Thread(new Runnable() {
					@Override
					public void run() {
						textFieldIdade.setText(calculaIdade(datePickerDataDeNascimento.getModel().getValue()));// TODO Auto-generated method stub
					}
				});
				thread.start();
			}
		});
	    
	    labelIdade.setLabelFor(textFieldIdade);
	    panelDataDeNascimento.add(labelIdade);
	    panelDataDeNascimento.add(textFieldIdade);
	    
	    JPanel panelEstadoCivil = new JPanel(new FlowLayout(FlowLayout.LEFT));
	    JLabel labelEstadoCivil = new JLabel("Estado Civil:              ");
	    
	    radioButtonCasado = new JRadioButton("Casado");
	    radioButtonCasado.setSelected(paciente != null && paciente.getEstadoCivil().equalsIgnoreCase("Casado"));
	    radioButtonDivorciado = new JRadioButton("Divorciado/Separado");
	    radioButtonDivorciado.setSelected(paciente != null && paciente.getEstadoCivil().equalsIgnoreCase("Divorciado/Separado"));
	    radioButtonSolteiro = new JRadioButton("Solteiro");
	    radioButtonSolteiro.setSelected(paciente != null && paciente.getEstadoCivil().equalsIgnoreCase("Solteiro"));
	    radioButtonViuvo = new JRadioButton("Viuvo");
	    radioButtonViuvo.setSelected(paciente != null && paciente.getEstadoCivil().equalsIgnoreCase("Viuvo"));
	    
	    panelEstadoCivil.add(labelEstadoCivil);
	    ButtonGroup  buttonGroupEstadoCivil = new ButtonGroup();
	    buttonGroupEstadoCivil.add(radioButtonCasado);
	    buttonGroupEstadoCivil.add(radioButtonDivorciado);
	    buttonGroupEstadoCivil.add(radioButtonSolteiro);
	    buttonGroupEstadoCivil.add(radioButtonViuvo);
	    panelEstadoCivil.add(radioButtonCasado);
	    panelEstadoCivil.add(radioButtonDivorciado);
	    panelEstadoCivil.add(radioButtonSolteiro);
	    panelEstadoCivil.add(radioButtonViuvo);
	    
	    JPanel panelEtnia = new JPanel(new FlowLayout(FlowLayout.LEFT));
	    JLabel labelEtnia = new JLabel("Etnia:                        ");
	    
	    radioButtonAmarela = new JRadioButton("Amarela (oriental)");
	    radioButtonAmarela.setSelected(paciente != null && paciente.getEstadoCivil().equalsIgnoreCase("Amarela (oriental)"));
	    radioButtonBranca = new JRadioButton("Branca");
	    radioButtonBranca.setSelected(paciente != null && paciente.getEstadoCivil().equalsIgnoreCase("Branca"));
	    radioButtonIndigena = new JRadioButton("Indigena");
	    radioButtonIndigena.setSelected(paciente != null && paciente.getEstadoCivil().equalsIgnoreCase("Indigena"));
	    radioButtonNegra = new JRadioButton("Negra");
	    radioButtonNegra.setSelected(paciente != null && paciente.getEstadoCivil().equalsIgnoreCase("Negra"));
	    radioButtonParda = new JRadioButton("Parda (mestica)");
	    radioButtonParda.setSelected(paciente != null && paciente.getEstadoCivil().equalsIgnoreCase("Parda (mestica)"));
	    
	    panelEtnia.add(labelEtnia);
	    ButtonGroup  buttonGroupEtnia = new ButtonGroup();
	    buttonGroupEtnia.add(radioButtonAmarela);
	    buttonGroupEtnia.add(radioButtonBranca);
	    buttonGroupEtnia.add(radioButtonIndigena);
	    buttonGroupEtnia.add(radioButtonNegra);
	    buttonGroupEtnia.add(radioButtonParda);
	    
	    panelEtnia.add(radioButtonAmarela);
	    panelEtnia.add(radioButtonBranca);
	    panelEtnia.add(radioButtonIndigena);
	    panelEtnia.add(radioButtonNegra);
	    panelEtnia.add(radioButtonParda);
	    
	    JPanel panelEndereco = new JPanel(new FlowLayout(FlowLayout.LEFT));
	    panelEndereco.add(new JLabel("Endereco Completo: "));
	    
	    JPanel panelRua = new JPanel(new FlowLayout(FlowLayout.LEFT));
	    JLabel  jLabelRua = new JLabel("Rua:      ");
	    textFieldRua = new JTextField(50);
	    textFieldRua.setText(paciente != null ? paciente.getEndereco() : "");
	    jLabelRua.setLabelFor(textFieldRua);
	    panelRua.add(jLabelRua);
	    panelRua.add(textFieldRua);
	    
	    JPanel panelBairro = new JPanel(new FlowLayout(FlowLayout.LEFT));
	    JLabel  jLabelBairro = new JLabel("Bairro:   ");
	    textFieldBairro = new JTextField(50);
	    textFieldBairro.setText(paciente != null ? paciente.getBairro() : "");
	    jLabelBairro.setLabelFor(textFieldBairro);
	    panelBairro.add(jLabelBairro);
	    panelBairro.add(textFieldBairro);
	    
	    JPanel panelEstado = new JPanel(new FlowLayout(FlowLayout.LEFT));
	    JLabel  jLabelEstado = new JLabel("Estado: ");
	    jComboEstados = new JComboBox<String>();
	    List<String> estados = FileUtil.getItensFromFile(Directory.FORM.getPath() + File.separator + "estados");
	    Collections.sort(estados);
	    for(String item : estados){
	    	jComboEstados.addItem(item);
	    }
	    
	    jLabelEstado.setLabelFor(jComboEstados);
	    panelEstado.add(jLabelEstado);
	    panelEstado.add(jComboEstados);

	    if(paciente != null && paciente.getEstado() != null)
	    	jComboEstados.setSelectedItem(paciente.getEstado());
	    
	    JPanel panelCEP = new JPanel(new FlowLayout(FlowLayout.LEFT));
	    JLabel  jLabelCEP = new JLabel("CEP:     ");
	    textFieldCEP = new JFormattedTextField(new MaskFormatter("#####-###"));
	    textFieldCEP.setColumns(50);
	    textFieldCEP.setText(paciente != null && paciente.getCep() != null ? paciente.getCep() : "");
	    
	    jLabelCEP.setLabelFor(textFieldCEP);
	    panelCEP.add(jLabelCEP);
	    panelCEP.add(textFieldCEP);
	    
	    panelDadosDemograficos.add(panelNome);
	    panelDadosDemograficos.add(panelSAME);
	    panelDadosDemograficos.add(panelDataDeAdmissao);
	    panelDadosDemograficos.add(panelSexo);
	    panelDadosDemograficos.add(panelDataDeNascimento);
	    panelDadosDemograficos.add(panelEstadoCivil);
	    panelDadosDemograficos.add(panelEtnia);
	    panelDadosDemograficos.add(panelEndereco);
	    panelDadosDemograficos.add(panelRua);
	    panelDadosDemograficos.add(panelBairro);
	    panelDadosDemograficos.add(panelEstado);
	    panelDadosDemograficos.add(panelCEP);

	    JPanel panelResidencial = new JPanel(new FlowLayout(FlowLayout.LEFT));
	    JLabel  jLabelResidencial = new JLabel("Telefone Residencial:  ");
	    textFieldResidencial = new JFormattedTextField(new MaskFormatter("(###) ## ####-####"));
	    textFieldResidencial.setColumns(50);
	    textFieldResidencial.setText(paciente != null && paciente.getTelefoneResidencial() != null ? paciente.getTelefoneResidencial() : "");
	    jLabelResidencial.setLabelFor(textFieldResidencial);
	    panelResidencial.add(jLabelResidencial);
	    panelResidencial.add(textFieldResidencial);
	    
	    JPanel panelComercial = new JPanel(new FlowLayout(FlowLayout.LEFT));
	    JLabel  jLabelComercial = new JLabel("Telefone Comercial:    ");
	    textFieldComercial = new JFormattedTextField(new MaskFormatter("(###) ## ####-####"));
	    textFieldComercial.setColumns(50);
	    textFieldComercial.setText(paciente != null && paciente.getTelefoneComercial() != null ? paciente.getTelefoneComercial() : "");
	    jLabelComercial.setLabelFor(textFieldComercial);
	    panelComercial.add(jLabelComercial);
	    panelComercial.add(textFieldComercial);

	    JPanel panelCelular = new JPanel(new FlowLayout(FlowLayout.LEFT));
	    JLabel  jLabelCelular = new JLabel("Telefone Celular:        ");
	    textFieldCelular = new JFormattedTextField(new MaskFormatter("(###) ## 9####-####"));
	    textFieldCelular.setColumns(50);
	    textFieldCelular.setText(paciente != null && paciente.getTelefoneCelular() != null ? paciente.getTelefoneCelular() : "");
	    jLabelCelular.setLabelFor(textFieldCelular);
	    panelCelular.add(jLabelCelular);
	    panelCelular.add(textFieldCelular);

	    JPanel panelRecados = new JPanel(new FlowLayout(FlowLayout.LEFT));
	    JLabel  jLabelRecados = new JLabel("Recados/Nome:          ");
	    textFieldRecados = new JTextField(50);
	    jLabelRecados.setLabelFor(textFieldRecados);
	    panelRecados.add(jLabelRecados);
	    panelRecados.add(textFieldRecados);
	    
	    JPanel panelProcedente = new JPanel(new FlowLayout(FlowLayout.LEFT));
	    JLabel  jLabelProcedente = new JLabel("Procedente de (ultima cidade onde morou): ");
	    textFieldProcedente = new JTextField(20); 
	    jLabelProcedente.setLabelFor(textFieldProcedente);
	    JLabel  jLabelHa = new JLabel("ha:");
	    textFieldHa = new JFormattedTextField(new MaskFormatter("###"));
	    textFieldHa.setColumns(5);
	    textFieldHa.setText(paciente != null && paciente.getHa() != null ? paciente.getHa().split("-")[0] : "");
	    jLabelHa.setLabelFor(textFieldHa);
	    JComboHa = new JComboBox<String>();
	    if(paciente != null && paciente.getHa() != null)
	    	JComboHa.setSelectedItem(paciente.getHa().split("-")[1]);
	    
	    JComboHa.addItem("Meses");
	    JComboHa.addItem("Anos");
	    
	    panelProcedente.add(jLabelProcedente);
	    panelProcedente.add(textFieldProcedente);
	    panelProcedente.add(jLabelHa);
	    panelProcedente.add(textFieldHa);
	    panelProcedente.add(JComboHa);
	    
	    JPanel panelProfissao = new JPanel(new FlowLayout(FlowLayout.LEFT));
	    JLabel  jLabelProfissao = new JLabel("Profissao:                   ");
	    textFieldProfissao = new JTextField(50);
	    textFieldProfissao.setText(paciente != null && paciente.getProfissao() != null ? paciente.getProfissao() : "");
	    jLabelProfissao.setLabelFor(textFieldProfissao);
	    panelProfissao.add(jLabelProfissao);
	    panelProfissao.add(textFieldProfissao);
	    
	    JPanel panelEscolaridade = new JPanel(new FlowLayout(FlowLayout.LEFT));
	    JLabel  jLabelEscolaridade = new JLabel("Escolaridade:              ");
	    
	    String[] escolaridade = (paciente != null && StringUtils.isNotBlank(paciente.getEscolaridade())) ? paciente.getEscolaridade().split("-") : null; 
	    jComboEscolaridade = new JComboBox<String>();
	    
	    for(String item : FileUtil.getItensFromFile(Directory.FORM.getPath() + File.separator + "escolaridade"))
	    	jComboEscolaridade.addItem(item);
	    
	    jComboEscolaridade.setSelectedItem(escolaridade != null ? escolaridade[0] : null);
	    
	    jComboCompleto = new JComboBox<String>();
	    jComboCompleto.addItem("completo");
	    jComboCompleto.addItem("incompleto");
	    jComboCompleto.setSelectedItem(escolaridade != null ? escolaridade[1] : null);
	    
	    jLabelEscolaridade.setLabelFor(jComboEscolaridade);
	    panelEscolaridade.add(jLabelEscolaridade);
	    panelEscolaridade.add(jComboEscolaridade);
	    
	    jComboEscolaridade.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String value = (String)jComboEscolaridade.getSelectedItem();
				if(!"naoestudou".equalsIgnoreCase(Normalize.normalizeName(value)))
					panelEscolaridade.add(jComboCompleto);
				else 
					panelEscolaridade.remove(jComboCompleto);
			}
		});

	    JPanel panelConvenio = new JPanel(new FlowLayout(FlowLayout.LEFT));
	    JLabel jLabelConvenio = new JLabel("Convênio:                  ");
	    
	    radioButtonConvenioSim = new JRadioButton("Sim");
	    radioButtonConvenioSim.setSelected(paciente != null && paciente.hasConvenio());
	    
	    radioButtonConvenioNao = new JRadioButton("Nao");
	    radioButtonConvenioNao.setSelected(paciente != null && !paciente.hasConvenio());
	    
	    ButtonGroup  buttonGroupConvenio = new ButtonGroup();
	    buttonGroupConvenio.add(radioButtonConvenioSim);
	    buttonGroupConvenio.add(radioButtonConvenioNao);
	    
	    panelConvenio.add(jLabelConvenio);
	    panelConvenio.add(radioButtonConvenioSim);
	    panelConvenio.add(radioButtonConvenioNao);
	    
	    panelDadosDemograficos2.add(panelResidencial);
	    panelDadosDemograficos2.add(panelComercial);
	    panelDadosDemograficos2.add(panelCelular);
	    panelDadosDemograficos2.add(panelRecados);
	    panelDadosDemograficos2.add(panelProcedente);
	    panelDadosDemograficos2.add(panelProfissao);
	    panelDadosDemograficos2.add(panelEscolaridade);
	    panelDadosDemograficos2.add(panelConvenio);
	    
	    JPanel panelDivide = new JPanel(new GridLayout(1,2));
	    JPanel jPanelEsquerda = new JPanel(new FlowLayout(FlowLayout.LEFT));
	    jPanelEsquerda.add(panelDadosDemograficos);
	    
	    JPanel jPanelDireita = new JPanel(new FlowLayout(FlowLayout.RIGHT));
	    jPanelDireita.add(panelDadosDemograficos2);
	    
	    panelDivide.add(jPanelEsquerda);
	    panelDivide.add(jPanelDireita);
	    

	    JPanel jPanelEmcima = new JPanel(new BorderLayout());
	    jPanelEmcima.add(new JLabel(), BorderLayout.NORTH);
	    jPanelEmcima.add(panelTitulo, BorderLayout.CENTER);
	    
	    JPanel panel = new JPanel();
	    panel.add(jPanelEmcima, BorderLayout.NORTH);
	    panel.add(panelDivide, BorderLayout.CENTER);
	    
	    return panel;
	}

	@Override
	public Paciente getPacienteDataFromScreen() {
		String nome = textFieldNome.getText();
		String same = textFieldSAME.getText();
		int idade = Integer.parseInt(textFieldIdade.getText());
		DateTime dataDeAdmissao = new DateTime((Date)datePickerDataDeAdmissao.getModel().getValue());         
		DateTime dataDeNascimento = new DateTime((Date)datePickerDataDeNascimento.getModel().getValue());
		String estadoCivil = radioButtonCasado.isSelected() ? radioButtonCasado.getText() : radioButtonDivorciado.isSelected() ? radioButtonCasado.getText() : "";
		String etnia = radioButtonAmarela.isSelected() ? radioButtonAmarela.getText() : radioButtonBranca.isSelected() ? radioButtonBranca.getText() : radioButtonIndigena.isSelected() ? radioButtonIndigena.getText() : radioButtonNegra.isSelected() ? radioButtonNegra.getText() : radioButtonParda.isSelected() ? radioButtonParda.getText() : "";
		String sexo = radioButtonMasculino.isSelected() ? radioButtonMasculino.getText() : radioButtonFeminino.isSelected() ? radioButtonFeminino.getText() : "";
		String endereco = textFieldRua.getText();
		String estado = (String)jComboEstados.getSelectedItem();
		String bairro = textFieldBairro.getText();
		String escolaridade = (String)jComboEscolaridade.getSelectedItem() + "-" + (String)jComboCompleto.getSelectedItem();
		boolean convenio = radioButtonConvenioSim.isSelected() ? true : false;
		String cep = textFieldCEP.getText();
		String celular = textFieldCelular.getText();
		String comercial = textFieldComercial.getText();
		String residencial = textFieldResidencial.getText();
		String recados = textFieldRecados.getText();
		String procedente = textFieldProcedente.getText();
		String profissao = textFieldProfissao.getText();
		String ha = textFieldHa.getText() + "-" + (String)JComboHa.getSelectedItem();
		
		paciente.setNome(nome);
		paciente.setSame(same);
		paciente.setIdade(idade);
		paciente.setDataDeAdmissao(dataDeAdmissao);
		paciente.setDataDeNascimento(dataDeNascimento);
		paciente.setEstadoCivil(estadoCivil);
		paciente.setEtnia(etnia);
		paciente.setSexo(sexo);
		paciente.setEndereco(endereco);
		paciente.setBairro(bairro);
		paciente.setEscolaridade(escolaridade);
		paciente.setConvenio(convenio);
		paciente.setEstado(estado);
		paciente.setCep(cep);
		paciente.setTelefoneCelular(celular);
		paciente.setTelefoneComercial(comercial);
		paciente.setTelefoneResidencial(residencial);
		paciente.setRecados(recados);
		paciente.setHa(ha);
		paciente.setProcedente(procedente);
		paciente.setProfissao(profissao);
		
		paciente.setActive(true);
		paciente.setUpdatedAt(new DateTime());
		paciente.setCreatedAt(paciente.getCreatedAt() != null ? paciente.getCreatedAt() : new DateTime());
		
		return paciente;
	}
}
