package br.com.tveiga.ui.cadastro;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.text.ParseException;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import net.sourceforge.jdatepicker.impl.JDatePickerImpl;
import br.com.tveiga.bean.Paciente;

public class Cadastro1Consulta extends CadastroTab {

	private Paciente paciente;
	private JRadioButton radioButtonRCUI;
	private JRadioButton radioButtonDC;
	private JRadioButton radioButtonColite;
	private JDatePickerImpl datePickerData1Consulta;
	private JTextField sintomasEram;
	private JDatePickerImpl datePickerDataDiagnostico;

	public Cadastro1Consulta(Paciente paciente) {
		this.paciente = paciente;
	}

	@Override
	protected JPanel createPanel() throws ParseException {
		JPanel jPanel = new JPanel();
		
		JPanel titulo = new JPanel(new FlowLayout(FlowLayout.CENTER));
		JLabel jLabel = new JLabel("Historico Paciente");
		titulo.add(jLabel);
		titulo.add(new JLabel());

		JPanel diagnosticoAtualPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel diagnosticoAtualLabel = new JLabel("Diagnostico Atual");
		diagnosticoAtualPanel.add(diagnosticoAtualLabel);
		
		radioButtonDC = new JRadioButton("DC");
	    radioButtonRCUI = new JRadioButton("RCUI");
	    radioButtonColite = new JRadioButton("COLITE INDETERMINADA");
	    
	    ButtonGroup  buttonGroupDiagnostico = new ButtonGroup();
	    buttonGroupDiagnostico.add(radioButtonDC);
	    buttonGroupDiagnostico.add(radioButtonRCUI);
	    buttonGroupDiagnostico.add(radioButtonColite);
		
	    diagnosticoAtualPanel.add(radioButtonDC);
	    diagnosticoAtualPanel.add(radioButtonRCUI);
	    diagnosticoAtualPanel.add(radioButtonColite);
	    
	    JPanel panelData1Consulta = new JPanel(new FlowLayout(FlowLayout.LEFT));
	    datePickerData1Consulta = createDatePicker();
	    
	    JLabel labelData1Consulta = new JLabel("Data da 1º Consulta:     ");
	    labelData1Consulta.setLabelFor(datePickerData1Consulta);
	    panelData1Consulta.add(labelData1Consulta);
	    panelData1Consulta.add(datePickerData1Consulta);
	    
	    diagnosticoAtualPanel.add(panelData1Consulta);
	    
	    JPanel panelSintomasEram = new JPanel(new FlowLayout(FlowLayout.LEFT));
	    JLabel labelSintomasEram = new JLabel("Os sintomas eram:     ");
	    sintomasEram = new JTextField(50);
	    labelSintomasEram.setLabelFor(sintomasEram);
	    panelSintomasEram.add(labelSintomasEram);
	    panelSintomasEram.add(sintomasEram);
	    diagnosticoAtualPanel.add(panelSintomasEram);
	    
	    JPanel panelDataDiagnostico = new JPanel(new FlowLayout(FlowLayout.LEFT));
	    JLabel labelDataDiagnostico = new JLabel("Data do Diagnostico:     ");
	    datePickerDataDiagnostico = createDatePicker();
	    labelDataDiagnostico.setLabelFor(datePickerDataDiagnostico);
	    panelDataDiagnostico.add(labelDataDiagnostico);
	    panelDataDiagnostico.add(datePickerDataDiagnostico);
	    
	    
	    diagnosticoAtualPanel.add(panelDataDiagnostico);
	    
	    
		jPanel.add(titulo, BorderLayout.NORTH);
		jPanel.add(diagnosticoAtualPanel, BorderLayout.CENTER);
		
		return jPanel;
	}

	@Override
	public Paciente getPacienteDataFromScreen() {
		return paciente;
	}

}
