package br.com.tveiga.ui.cadastro;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.function.Consumer;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import net.sourceforge.jdatepicker.impl.JDatePickerImpl;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import br.com.tveiga.bean.Medicamento;
import br.com.tveiga.bean.Paciente;
import br.com.tveiga.bean.Patologia;
import br.com.tveiga.bean.PrincipioAtivo;
import br.com.tveiga.business.PatologiaBussiness;
import br.com.tveiga.business.PrincipioAtivoBussiness;
import br.com.tveiga.util.ButtonIcon;
import br.com.tveiga.util.Directory;
import br.com.tveiga.util.FileUtil;

public class CadastroMedicamento extends CadastroTab {

	private JPanel panelMedicacaoList;

	private JTextField jTextFieldMedicacaoConcomitante;
	private JTextField jTextFieldClasseTerapeutica;
	private JTextField jTextFieldIndicacao;
	private JDatePickerImpl datePickerDataDeInicio;
	private JDatePickerImpl datePickerDataDeTermino;
	private JTextField jTextFieldPeriodoDeAdmistracao;
	private JComboBox<String> jComboBoxViaDeAdmistracao;
	private JComboBox<String> jComboBoxConcentracaoApresentacao;
	private JTextField jTextFieldPosologia;
	private PrincipioAtivoBussiness principioAtivoBussiness;

	private JTextField jTextFieldPrincipioAtivo;
	private JScrollPane scroll; 
	private JFrame frameSelection;
	private List<PrincipioAtivo> principiosAtivos;
	private List<Medicamento> medicamentos;
	
	private JPanel panelMedicacao;

	private List<Patologia> patologias;

	private PatologiaBussiness patologiaBussiness;
	private Paciente paciente;
	
	
	public CadastroMedicamento(Paciente paciente) {
		this.paciente = paciente;
	}

	@Override
	protected JPanel createPanel() {
		medicamentos = new ArrayList<Medicamento>();
		
		JPanel jpanelGeral = new JPanel(new BorderLayout());
		JPanel panelComp = new JPanel(new GridLayout(0, 1));
		panelMedicacao = new JPanel(new GridLayout(0, 6));

		patologiaBussiness = new PatologiaBussiness();
		
		panelMedicacao.add(new JLabel("Medic. Concomitante"));
		panelMedicacao.add(new JLabel("Principio Ativo"));
		panelMedicacao.add(new JLabel("Classe Terapeutica"));
		panelMedicacao.add(new JLabel("Indicacao"));
		panelMedicacao.add(new JLabel("Inicio"));
		panelMedicacao.add(new JLabel());
		
		jTextFieldMedicacaoConcomitante = new JTextField(25);
		jTextFieldPrincipioAtivo = new JTextField(25);
		jTextFieldClasseTerapeutica = new JTextField(25);
		jTextFieldClasseTerapeutica.setEditable(false);
		jTextFieldIndicacao = new JTextField(25);
		datePickerDataDeInicio = createDatePicker();
		datePickerDataDeTermino = createDatePicker();
		jTextFieldPeriodoDeAdmistracao = new JTextField(25);
		jComboBoxViaDeAdmistracao = new JComboBox<String>();
		jComboBoxConcentracaoApresentacao = new JComboBox<String>();
		principioAtivoBussiness = new PrincipioAtivoBussiness();
		jTextFieldPosologia = new JTextField(25);
		
		for(String viaadministracao : FileUtil.getItensFromFile(Directory.FORM.getPath() + File.separator + "viadeadministracao"))
			jComboBoxViaDeAdmistracao.addItem(viaadministracao);
		
		for(String concentracao : FileUtil.getItensFromFile(Directory.FORM.getPath() + File.separator + "concentracaoapresentacao"))
			jComboBoxConcentracaoApresentacao.addItem(concentracao);
			
		panelMedicacao.add(jTextFieldMedicacaoConcomitante);
		panelMedicacao.add(jTextFieldPrincipioAtivo);
		panelMedicacao.add(jTextFieldClasseTerapeutica);
		panelMedicacao.add(jTextFieldIndicacao);
		panelMedicacao.add(datePickerDataDeInicio);
		panelMedicacao.add(new JLabel());
		
		panelMedicacao.add(new JLabel());
		panelMedicacao.add(new JLabel());
		panelMedicacao.add(new JLabel());
		panelMedicacao.add(new JLabel());
		panelMedicacao.add(new JLabel());
		panelMedicacao.add(new JLabel());
		
		panelMedicacao.add(new JLabel("Termino"));
		panelMedicacao.add(new JLabel("Periodo de Admistracao"));
		panelMedicacao.add(new JLabel("Via de Admistracao"));
		panelMedicacao.add(new JLabel("Conc./Apresentacao"));
		panelMedicacao.add(new JLabel("Posologia"));
		panelMedicacao.add(new JLabel());
		
		datePickerDataDeTermino.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Thread thread = new Thread(new Runnable() {
					@Override
					public void run() {
						Date inicio = datePickerDataDeInicio.getModel().getValue() == null ? new Date() : (Date) datePickerDataDeInicio.getModel().getValue();
						Date fim = datePickerDataDeTermino.getModel().getValue() == null ? new Date() : (Date) datePickerDataDeTermino.getModel().getValue();
						String periodoDeAdmistracao = calculaPeriodo(inicio, fim);
						jTextFieldPeriodoDeAdmistracao.setText(periodoDeAdmistracao);
					}
				});
				thread.start();
			}
		});
		
		
		
		panelMedicacao.add(datePickerDataDeTermino);
		panelMedicacao.add(jTextFieldPeriodoDeAdmistracao);
		panelMedicacao.add(jComboBoxViaDeAdmistracao);
		panelMedicacao.add(jComboBoxConcentracaoApresentacao);
		panelMedicacao.add(jTextFieldPosologia);
		
		JButton jButtonAdd = new JButton(new ImageIcon(ButtonIcon.BUTTON_ADD.getPath()));
		jButtonAdd.setText("Adicionar Registro");
		
		panelMedicacao.add(jButtonAdd);
		
		panelMedicacao.add(new JLabel());
		panelMedicacao.add(new JLabel());
		panelMedicacao.add(new JLabel());
		panelMedicacao.add(new JLabel());
		panelMedicacao.add(new JLabel());
		panelMedicacao.add(new JLabel());
		
		panelMedicacaoList = new JPanel();
		
		principiosAtivos = principioAtivoBussiness.getAll();
		
		jTextFieldPrincipioAtivo.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				new Thread(new Runnable() {
					
					@Override
					public void run() {
						JList<PrincipioAtivo> texto = new JList<PrincipioAtivo>();
						texto.addListSelectionListener(new ListSelectionListener() {
							@Override
							public void valueChanged(ListSelectionEvent e) {
								PrincipioAtivo principioAtivo = texto.getSelectedValue();
								jTextFieldPrincipioAtivo.setText(principioAtivo.getNome());
								jTextFieldClasseTerapeutica.setText(principioAtivo.getClasseTerapeutica().getNome());
								jTextFieldPrincipioAtivo.transferFocus();
								frameSelection.dispose();
								frameSelection = null;
							}
						});
						
						List<PrincipioAtivo> list = new ArrayList<PrincipioAtivo>();
						principiosAtivos.forEach(new Consumer<PrincipioAtivo>() {
						   @Override
     						public void accept(PrincipioAtivo principioAtivo) {
							  if(principioAtivo.getNome().toLowerCase().startsWith(jTextFieldPrincipioAtivo.getText().toLowerCase()))
								  list.add(principioAtivo);
							}
	    				});
						   
					 texto.setListData(list.toArray(new PrincipioAtivo[list.size()]));  

					 if(frameSelection != null){
						frameSelection.dispose();
						frameSelection = null;
					 }
						 
  	                 frameSelection = new JFrame();
				     frameSelection.setResizable(false);
				     JScrollPane scrollpane = new JScrollPane(texto);
				     Container pane = frameSelection.getContentPane(); 
    			     pane.setLayout(new FlowLayout(FlowLayout.LEFT));
				     scrollpane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
				     scrollpane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
				     pane.add(scrollpane);
				     frameSelection.pack();
				     frameSelection.setAlwaysOnTop(true);
				     Point point = jTextFieldPrincipioAtivo.getLocationOnScreen();
				     frameSelection.setLocation((int)point.getX(), (int)(point.getY() + jTextFieldPrincipioAtivo.getHeight()));
				     frameSelection.setVisible(true);
				     jTextFieldPrincipioAtivo.setFocusable(true);
				     jTextFieldPrincipioAtivo.requestFocus();
				     
				  }
				}).start();
			}
		});
		
		patologias =  patologiaBussiness.getAll();
		
		
		jTextFieldIndicacao.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				new Thread(new Runnable() {
					
					@Override
					public void run() {
						JList<Patologia> texto = new JList<Patologia>();
						texto.addListSelectionListener(new ListSelectionListener() {
							@Override
							public void valueChanged(ListSelectionEvent e) {
								patologia = texto.getSelectedValue();
								jTextFieldIndicacao.setText(patologia.getNome());
								jTextFieldIndicacao.transferFocus();
								frameSelection.dispose();
								frameSelection = null;
							}
						});
						
						List<Patologia> list = new ArrayList<Patologia>();
						patologias.forEach(new Consumer<Patologia>() {
						   @Override
     						public void accept(Patologia patologia) {
							  if(patologia.getNome().toLowerCase().startsWith(jTextFieldIndicacao.getText().toLowerCase()))
								  list.add(patologia);
							}
	    				});
						   
					 texto.setListData(list.toArray(new Patologia[list.size()]));  

					 if(frameSelection != null){
						frameSelection.dispose();
						frameSelection = null;
					 }
						 
  	                 frameSelection = new JFrame();
				     frameSelection.setResizable(false);
				     JScrollPane scrollpane = new JScrollPane(texto);
				     Container pane = frameSelection.getContentPane(); 
    			     pane.setLayout(new FlowLayout(FlowLayout.LEFT));
				     scrollpane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
				     scrollpane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
				     pane.add(scrollpane);
				     frameSelection.pack();
				     frameSelection.setAlwaysOnTop(true);
				     Point point = jTextFieldIndicacao.getLocationOnScreen();
				     frameSelection.setLocation((int)point.getX(), (int)(point.getY() + jTextFieldIndicacao.getHeight()));
				     frameSelection.setVisible(true);
				     jTextFieldIndicacao.setFocusable(true);
				     jTextFieldIndicacao.requestFocus();
				     
				  }
				}).start();
			}
		});

		
		jButtonAdd.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				

				
				String medicacaoConcomitante = jTextFieldMedicacaoConcomitante.getText();
				String principioAtivo = jTextFieldPrincipioAtivo.getText();
				String classeTerapeutica = jTextFieldClasseTerapeutica.getText();
				String indicacao = jTextFieldIndicacao.getText();
				Date inicio = datePickerDataDeInicio.getModel().getValue() == null ? new Date() : (Date) datePickerDataDeInicio.getModel().getValue();
				Date fim = datePickerDataDeTermino.getModel().getValue() == null ? new Date() : (Date) datePickerDataDeTermino.getModel().getValue();
				String dataInicio =  inicio == null ? getNow() : new DateTime(inicio).toString(DateTimeFormat.mediumDate());
				String dataTermino = fim == null ? getNow() : new DateTime(fim).toString(DateTimeFormat.mediumDate());
				String periodoDeAdmistracao = jTextFieldPeriodoDeAdmistracao.getText();
				periodoDeAdmistracao = StringUtils.isBlank(periodoDeAdmistracao) ? calculaPeriodo(inicio, fim) : periodoDeAdmistracao;
				String viaDeAdmistracao = (String)jComboBoxViaDeAdmistracao.getSelectedItem();
				String concentracaoApresentacao = (String)jComboBoxConcentracaoApresentacao.getSelectedItem();
				String posologia = jTextFieldPosologia.getText();
				
				boolean sucess = adicionaRegistro(panelMedicacaoList, scroll, panelMedicacao.getSize().width, 400, StringUtils.isBlank(medicacaoConcomitante) ? "nao preenchido" : medicacaoConcomitante, principioAtivo, classeTerapeutica, indicacao, dataInicio, dataTermino, periodoDeAdmistracao, viaDeAdmistracao, concentracaoApresentacao, posologia);
				
				if(sucess){
					
					Medicamento medicamento = new Medicamento();
					
					medicamento.setNome(medicacaoConcomitante);
					
					medicamento.setActive(true);
					medicamento.setUpdatedAt(new DateTime());
					medicamento.setCreatedAt(medicamento.getCreatedAt() != null ? medicamento.getCreatedAt() : new DateTime());
					
					medicamentos.add(medicamento);
					
					jTextFieldMedicacaoConcomitante.setText("");
					jTextFieldPrincipioAtivo.setText("");
					jTextFieldClasseTerapeutica.setText("");
					jTextFieldIndicacao.setText("");
					datePickerDataDeInicio.getModel().setValue(null);
					datePickerDataDeTermino.getModel().setValue(null);
					jTextFieldPeriodoDeAdmistracao.setText("");
					jComboBoxViaDeAdmistracao.setSelectedItem(null);
					jComboBoxConcentracaoApresentacao.setSelectedItem(null);
					jTextFieldPosologia.setText("");
					
					jTextFieldPrincipioAtivo.requestFocus();
				}
				
			}
		});
		
		BoxLayout boxLayout = new BoxLayout(panelMedicacaoList, BoxLayout.Y_AXIS);
		panelMedicacaoList.setLayout(boxLayout);

		scroll = new JScrollPane(panelMedicacaoList);
		scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

		panelComp.add(scroll);
		jpanelGeral.add(panelMedicacao, BorderLayout.NORTH);
		jpanelGeral.add(panelComp, BorderLayout.CENTER);

		
		return jpanelGeral;
	}

	@Override
	public Paciente getPacienteDataFromScreen() {

		

		
		paciente.getHistorico().getMedicamentos().addAll(medicamentos);
		paciente.setUpdatedAt(new DateTime());
		
		return paciente;
	}

	
}
