package br.com.tveiga.ui.cadastro;

import java.awt.GridLayout;
import java.text.ParseException;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;

import br.com.tveiga.bean.Paciente;
import br.com.tveiga.bean.PersistentEntity;
import br.com.tveiga.business.Business;
import br.com.tveiga.business.PacienteBusiness;
import br.com.tveiga.factory.CadastroTabFactory;
import br.com.tveiga.log.Level;
import br.com.tveiga.log.Log;

public final class CadastroPaciente extends CadastroFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Paciente paciente;	
	private CadastroTabFactory tabFactory;
	private CadastroTab tabConsulta;
	private CadastroTab tabDados;
	private CadastroTab tabHistorico;
	private CadastroTab tabExameImagem;
	private CadastroTab tabExameLaboral;
	private CadastroTab tab1consulta;
	private CadastroTab tabInterConsultas;
	private CadastroTab tabMedicamento;
	
	public CadastroPaciente() {
		super("Paciente");
	}
	
	@Override
	protected JPanel createFieldsPanel() {
		JPanel jPanel = new JPanel(new GridLayout());
		JTabbedPane tabbedPane = new JTabbedPane(SwingConstants.TOP);
		
		tabFactory = CadastroTabFactory.getInstance(paciente);
		
		tabDados = tabFactory.getCadastroTab("dadosdemograficos");
		tabConsulta = tabFactory.getCadastroTab("consulta");
		tabHistorico = tabFactory.getCadastroTab("historico");
		tabExameImagem = tabFactory.getCadastroTab("exameimagem");
		tabExameLaboral = tabFactory.getCadastroTab("examelaboral");
		tab1consulta = tabFactory.getCadastroTab("1consulta");
		tabInterConsultas = tabFactory.getCadastroTab("interconsultas");
		tabMedicamento = tabFactory.getCadastroTab("medicamento");
		
		try {
			tabbedPane.addTab("Consultas", tabConsulta.createPanel());
			tabbedPane.addTab("Dados Demograficos", tabDados.createPanel());
			tabbedPane.addTab("Historico Medico / Anamnese", tabHistorico.createPanel());
			tabbedPane.addTab("Medicacoes Concomitantes  Tratamentos Anteriores para Doenças Intestinais", tabMedicamento.createPanel());
			tabbedPane.addTab("Exames de Imagem (COLONOSCOPIA e EDA)", tabExameImagem.createPanel());
			tabbedPane.addTab("Exames de Imagem (US,TOMO, RM,TI)", tabExameLaboral.createPanel());
			tabbedPane.addTab("1º Consulta", tab1consulta.createPanel());
			tabbedPane.addTab("InterConsultas / Avaliacao", tabInterConsultas.createPanel());
		} catch (ParseException e) {
			Log.log(CadastroPaciente.class.getSimpleName(), 46, e.getMessage(), Level.ERROR);
		}
		jPanel.add(tabbedPane);

		setPrint(this);
		setPersistentEntityCurrent(paciente);
		return jPanel;
	}

	@Override
	protected PersistentEntity getObjectFromScreen() {
		paciente = tabDados.getPacienteDataFromScreen();
		return paciente;
	}

	@Override
	protected Business getBusiness() {
		return new PacienteBusiness();
	}

}
