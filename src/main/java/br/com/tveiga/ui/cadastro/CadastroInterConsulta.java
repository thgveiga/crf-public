package br.com.tveiga.ui.cadastro;

import java.text.ParseException;

import javax.swing.JPanel;

import br.com.tveiga.bean.Paciente;

public class CadastroInterConsulta extends CadastroTab {

	Paciente paciente;
	
	public CadastroInterConsulta(Paciente paciente) {
		this.paciente = paciente;
	}

	@Override
	protected JPanel createPanel() throws ParseException {
		JPanel jPanel = new JPanel();
		return jPanel;
	}

	@Override
	public Paciente getPacienteDataFromScreen() {
		return paciente;
	}

}
