package br.com.tveiga.ui.cadastro;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import net.sourceforge.jdatepicker.impl.JDatePickerImpl;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import br.com.tveiga.bean.ExameLaboral;
import br.com.tveiga.bean.Paciente;
import br.com.tveiga.util.ButtonIcon;


public class CadastroExameLaboral extends CadastroTab {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Paciente paciente;
	
	private JScrollPane scroll;
	private JPanel lista;
	private List<ExameLaboral> examesLaborais = new ArrayList<ExameLaboral>();
	
	public CadastroExameLaboral(Paciente paciente) {
		this.paciente = paciente;
	}

	@Override
	protected JPanel createPanel() {
		JPanel jPanel = new JPanel();
		
		JLabel jLabel = new JLabel("US, TOMO, RESSONANCIA, TRANSITO - DESCREVA APENAS OS DADOS POSITIVOS.");
		JPanel titulo = new JPanel(new FlowLayout(FlowLayout.CENTER));
		titulo.add(jLabel);
		
		JPanel panelCampos = new JPanel(new FlowLayout(FlowLayout.LEFT));
		
		JLabel dataLabel = new JLabel("Data:");
	    JDatePickerImpl dataPicker = createDatePicker();
	    dataLabel.setLabelFor(dataPicker);
	    
		panelCampos.add(dataLabel);
		panelCampos.add(dataPicker);
		
		JCheckBox checkBoxUS = new JCheckBox("US");
		JCheckBox checkBoxTOMO = new JCheckBox("TOMO");
		JCheckBox checkBoxRM = new JCheckBox("RM");
		JCheckBox checkBoxTI = new JCheckBox("TI");
		
		panelCampos.add(checkBoxUS);
		panelCampos.add(checkBoxTOMO);
		panelCampos.add(checkBoxRM);
		panelCampos.add(checkBoxTI);
		
		JTextArea textArea = new JTextArea(2,70);
		textArea.setLineWrap(true);
		
		JScrollPane textAreaBar = new JScrollPane(textArea);
		textAreaBar.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		textAreaBar.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		
		panelCampos.add(textAreaBar);
		
	    JPanel buttonPanel = new JPanel();
		JButton jButtonAdd = new JButton(new ImageIcon(ButtonIcon.BUTTON_ADD.getPath()));
		jButtonAdd.setText("Adicionar Registro");
		buttonPanel.add(jButtonAdd);

		panelCampos.add(buttonPanel);
		
		lista = new JPanel();
		
		BoxLayout boxLayout = new BoxLayout(lista, BoxLayout.Y_AXIS);
		lista.setLayout(boxLayout);
		scroll = new JScrollPane(lista);
		
		scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

		jPanel.add(titulo, BorderLayout.NORTH);
		jPanel.add(panelCampos, BorderLayout.CENTER);
		jPanel.add(scroll);
		
		jButtonAdd.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Date data = dataPicker.getModel().getValue() == null ? new Date() : (Date) dataPicker.getModel().getValue();
				String dataStr =  data == null ? getNow() : new DateTime(data).toString(DateTimeFormat.mediumDate());
				String descricao = textArea.getText();
				descricao = StringUtils.isBlank(descricao) ? "sem descricao" : descricao;
				
				String exame = checkBoxRM.isSelected() ? checkBoxRM.getText() : " ";
				exame += checkBoxTOMO.isSelected() ? checkBoxTOMO.getText() : " ";
				exame += checkBoxTI.isSelected() ? checkBoxTI.getText() : " ";
				exame += checkBoxUS.isSelected() ? checkBoxUS.getText() : " ";
				
				boolean sucess = adicionaRegistro(lista, scroll, titulo.getSize().width, 300, dataStr, descricao, exame);
				if(sucess){
					ExameLaboral exameLaboral = new ExameLaboral();
					exameLaboral.setActive(true);
					exameLaboral.setUpdatedAt(new DateTime());
					exameLaboral.setCreatedAt(new DateTime());
					exameLaboral.setNome(exame);
					exameLaboral.setDescricao(descricao);
					exameLaboral.setData(new DateTime(data));
					
					examesLaborais.add(exameLaboral);
					
					dataPicker.getModel().setValue(null);
					textArea.setText("");
					checkBoxRM.setSelected(false);
					checkBoxTI.setSelected(false);
					checkBoxUS.setSelected(false);
					checkBoxTOMO.setSelected(false);
				}
			}
		});
		return jPanel;
	}

	@Override
	public Paciente getPacienteDataFromScreen() {
		return paciente;
	}
}
