package br.com.tveiga.bean;

import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.joda.time.DateTime;

@Entity
@Table(name="principioativo")
public class PrincipioAtivo implements PersistentEntity {

	@Id
	@GeneratedValue
	private Integer id;
	
	@Column
	private String nome;

    @OneToOne
    @JoinColumn(name="classeterapeutica_id", nullable=true)
	private ClasseTerapeutica classeTerapeutica;

    @ManyToMany(cascade=CascadeType.ALL, mappedBy="principiosAtivos")
	private List<Medicamento> medicamentos;
    
	@Column
	private Calendar created_at;
	
	@Column
	private Calendar updated_at;
	
	@Column
	private boolean active;
	
	@Override
	public Integer getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public ClasseTerapeutica getClasseTerapeutica() {
		return classeTerapeutica;
	}

	public void setClasseTerapeutica(ClasseTerapeutica classeTerapeutica) {
		this.classeTerapeutica = classeTerapeutica;
	}

	public List<Medicamento> getMedicamentos() {
		return medicamentos;
	}

	public void setMedicamentos(List<Medicamento> medicamentos) {
		this.medicamentos = medicamentos;
	}

	@Override
	public DateTime getCreatedAt() {
		return new DateTime(created_at);
	}

	@Override
	public void setCreatedAt(DateTime created_at) {
		this.created_at = created_at.toGregorianCalendar();
	}

	@Override
	public DateTime getUpdatedAt() {
		return new DateTime(updated_at);
	}

	@Override
	public void setUpdatedAt(DateTime updated_at) {
		this.updated_at = updated_at.toGregorianCalendar();
	}

	@Override
	public boolean isActive() {
		return active;
	}

	@Override
	public void setActive(boolean active) {
		this.active = active;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((classeTerapeutica == null) ? 0 : classeTerapeutica
						.hashCode());
		result = prime * result
				+ ((medicamentos == null) ? 0 : medicamentos.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PrincipioAtivo other = (PrincipioAtivo) obj;
		if (classeTerapeutica == null) {
			if (other.classeTerapeutica != null)
				return false;
		} else if (!classeTerapeutica.equals(other.classeTerapeutica))
			return false;
		if (medicamentos == null) {
			if (other.medicamentos != null)
				return false;
		} else if (!medicamentos.equals(other.medicamentos))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return nome;
	}

}
