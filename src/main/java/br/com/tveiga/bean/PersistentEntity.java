package br.com.tveiga.bean;

import org.joda.time.DateTime;

public interface PersistentEntity {
	
	Integer getId();
	
	boolean isActive();
	
	void setActive(boolean active);
	
	DateTime getCreatedAt();
	
	void setCreatedAt(DateTime created_at);
	
	DateTime getUpdatedAt();
	
	void setUpdatedAt(DateTime updated_at);
}
