package br.com.tveiga.bean;

import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.joda.time.DateTime;

@Entity
@Table(name="historico")
public class Historico implements PersistentEntity {

	@Id
	@GeneratedValue
	private Integer id;

	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
    @JoinTable(name="historico_patologia", joinColumns={@JoinColumn(name="historico_id", referencedColumnName="id")}, inverseJoinColumns={@JoinColumn(name="patologia_id", referencedColumnName="id")})	
	private List<Patologia> patologias;

	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
    @JoinTable(name="historico_medicamento", joinColumns={@JoinColumn(name="historico_id", referencedColumnName="id")}, inverseJoinColumns={@JoinColumn(name="medicamento_id", referencedColumnName="id")})
	private List<Medicamento> medicamentos;

	@Column
	private Calendar created_at;

	@Column
	private Calendar updated_at;

	@Column
	private boolean active;

	@Override
	public Integer getId() {
		return id;
	}

	public List<Patologia> getPatologias() {
		return patologias;
	}

	public void setPatologias(List<Patologia> patologias) {
		this.patologias = patologias;
	}

	public List<Medicamento> getMedicamentos() {
		return medicamentos;
	}

	public void setMedicamentos(List<Medicamento> medicamentos) {
		this.medicamentos = medicamentos;
	}

	@Override
	public DateTime getCreatedAt() {
		return new DateTime(created_at);
	}

	@Override
	public void setCreatedAt(DateTime created_at) {
		this.created_at = created_at.toGregorianCalendar();
	}

	@Override
	public DateTime getUpdatedAt() {
		return new DateTime(updated_at);
	}

	@Override
	public void setUpdatedAt(DateTime updated_at) {
		this.updated_at = updated_at.toGregorianCalendar();
	}

	@Override
	public boolean isActive() {
		return this.active;
	}

	@Override
	public void setActive(boolean active) {
		this.active = active;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((medicamentos == null) ? 0 : medicamentos.hashCode());
		result = prime * result
				+ ((patologias == null) ? 0 : patologias.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Historico other = (Historico) obj;
		if (medicamentos == null) {
			if (other.medicamentos != null)
				return false;
		} else if (!medicamentos.equals(other.medicamentos))
			return false;
		if (patologias == null) {
			if (other.patologias != null)
				return false;
		} else if (!patologias.equals(other.patologias))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Historico [id=" + id + ", patologias=" + patologias
				+ ", medicamentos=" + medicamentos + ", created_at="
				+ created_at + ", updated_at=" + updated_at + ", active="
				+ active + "]";
	}

}
