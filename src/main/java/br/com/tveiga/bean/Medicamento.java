package br.com.tveiga.bean;

import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.joda.time.DateTime;

@Entity
@Table(name="medicamento")
public class Medicamento implements PersistentEntity {

	@Id
	@GeneratedValue
	private Integer id;

	@Column
	private String nome;

	@Column
	private String posologia;

	@Column
	private String dosagem;

	@Column
	private Calendar dataInicio;

	@Column
	private Calendar dataTermino;

	@OneToOne(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	@JoinColumn(name="patologia_id", nullable=true)
	private Patologia patologia;

	@ManyToMany(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
    @JoinTable(name="medicamento_principioativo", joinColumns={@JoinColumn(name="medicamento_id", referencedColumnName="id")}, inverseJoinColumns={@JoinColumn(name="principioativo_id", referencedColumnName="id")})
	private List<PrincipioAtivo> principiosAtivos;

	@Column
	private Calendar created_at;

	@Column
	private Calendar updated_at;

	@Column
	private boolean active;

	@Override
	public Integer getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getPosologia() {
		return posologia;
	}

	public void setPosologia(String posologia) {
		this.posologia = posologia;
	}

	public String getDosagem() {
		return dosagem;
	}

	public void setDosagem(String dosagem) {
		this.dosagem = dosagem;
	}

	public Calendar getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Calendar dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Calendar getDataTermino() {
		return dataTermino;
	}

	public void setDataTermino(Calendar dataTermino) {
		this.dataTermino = dataTermino;
	}

	public Patologia getPatologia() {
		return patologia;
	}

	public void setPatologia(Patologia patologia) {
		this.patologia = patologia;
	}

	public List<PrincipioAtivo> getPrincipiosAtivos() {
		return principiosAtivos;
	}

	public void setPrincipioAtivo(List<PrincipioAtivo> principiosAtivos) {
		this.principiosAtivos = principiosAtivos;
	}

	@Override
	public DateTime getCreatedAt() {
		return new DateTime(created_at);
	}

	@Override
	public void setCreatedAt(DateTime created_at) {
		this.created_at = created_at.toGregorianCalendar();
	}

	@Override
	public DateTime getUpdatedAt() {
		return new DateTime(updated_at);
	}

	@Override
	public void setUpdatedAt(DateTime updated_at) {
		this.updated_at = updated_at.toGregorianCalendar();
	}

	@Override
	public boolean isActive() {
		return this.active;
	}

	@Override
	public void setActive(boolean active) {
		this.active = active;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((dataInicio == null) ? 0 : dataInicio.hashCode());
		result = prime * result
				+ ((dataTermino == null) ? 0 : dataTermino.hashCode());
		result = prime * result + ((dosagem == null) ? 0 : dosagem.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result
				+ ((patologia == null) ? 0 : patologia.hashCode());
		result = prime * result
				+ ((posologia == null) ? 0 : posologia.hashCode());
		result = prime * result
				+ ((principiosAtivos == null) ? 0 : principiosAtivos.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Medicamento other = (Medicamento) obj;
		if (dataInicio == null) {
			if (other.dataInicio != null)
				return false;
		} else if (!dataInicio.equals(other.dataInicio))
			return false;
		if (dataTermino == null) {
			if (other.dataTermino != null)
				return false;
		} else if (!dataTermino.equals(other.dataTermino))
			return false;
		if (dosagem == null) {
			if (other.dosagem != null)
				return false;
		} else if (!dosagem.equals(other.dosagem))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (patologia == null) {
			if (other.patologia != null)
				return false;
		} else if (!patologia.equals(other.patologia))
			return false;
		if (posologia == null) {
			if (other.posologia != null)
				return false;
		} else if (!posologia.equals(other.posologia))
			return false;
		if (principiosAtivos == null) {
			if (other.principiosAtivos != null)
				return false;
		} else if (!principiosAtivos.equals(other.principiosAtivos))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Medicamento [id=" + id + ", nome=" + nome + ", posologia="
				+ posologia + ", dosagem=" + dosagem + ", dataInicio="
				+ dataInicio + ", dataTermino=" + dataTermino + ", patologia="
				+ patologia + ", principioAtivo=" + principiosAtivos
				+ ", created_at=" + created_at + ", updated_at=" + updated_at
				+ ", active=" + active + "]";
	}

}
