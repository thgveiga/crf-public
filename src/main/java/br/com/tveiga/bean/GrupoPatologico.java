package br.com.tveiga.bean;

import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.joda.time.DateTime;

@Entity
@Table(name="grupopatologico")
public class GrupoPatologico implements PersistentEntity{
	
	@Id
	@GeneratedValue
	private Integer id;
	
	@Column
	private String nome;

	@OneToMany(cascade=CascadeType.ALL, mappedBy="grupoPatologico")
	private List<Patologia> patologias;
	
	@Column
	private Calendar created_at;
	
	@Column
	private Calendar updated_at;

	@Column
	private boolean active;

	public Integer getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public List<Patologia> getPatologias() {
		return patologias;
	}
	
	public void setPatologias(List<Patologia> patologias) {
		this.patologias = patologias;
	}

	@Override
	public DateTime getCreatedAt() {
		return new DateTime(created_at);
	}

	@Override
	public void setCreatedAt(DateTime created_at) {
		this.created_at = created_at.toGregorianCalendar();
	}
	
	@Override
	public DateTime getUpdatedAt() {
		return new DateTime(updated_at);
	}

	@Override
	public void setUpdatedAt(DateTime updated_at) {
		this.updated_at = updated_at.toGregorianCalendar();
	}
	
	@Override
	public boolean isActive() {
		return this.active;
	}

	@Override
	public void setActive(boolean active) {
		this.active = active;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result
				+ ((patologias == null) ? 0 : patologias.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GrupoPatologico other = (GrupoPatologico) obj;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (patologias == null) {
			if (other.patologias != null)
				return false;
		} else if (!patologias.equals(other.patologias))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "GrupoPatologico [id=" + id + ", nome=" + nome + ", patologias="
				+ patologias + ", created_at=" + created_at + ", updated_at="
				+ updated_at + ", active=" + active + "]";
	}
	
}
