package br.com.tveiga.bean;

import java.util.Calendar;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.joda.time.DateTime;

@Entity
@Table(name="consulta")
public class Consulta implements PersistentEntity{
	
	@Id
	@GeneratedValue
	private Integer id;
	
	@Column
	private Calendar data;

	@ManyToOne(cascade=CascadeType.ALL)
	private Paciente paciente;

	@Column
	private boolean active;
	
	@Column
	private Calendar created_at;
	
	@Column
	private Calendar updated_at;

	
	public Integer getId() {
		return id;
	}

	public DateTime getData() {
		return new DateTime(data);
	}

	public void setData(DateTime data) {
		this.data = data.toGregorianCalendar();
	}
	
	public Paciente getPaciente(){
		return paciente;
	}
	
	public void setPaciente(Paciente paciente){
		this.paciente = paciente;
	}
	
	@Override
	public boolean isActive() {
		return this.active;
	}

	@Override
	public void setActive(boolean active) {
		this.active = active;
	}
	
	@Override
	public DateTime getCreatedAt() {
		return new DateTime(created_at);
	}

	@Override
	public void setCreatedAt(DateTime created_at) {
		this.created_at = created_at.toGregorianCalendar();
	}
	
	@Override
	public DateTime getUpdatedAt() {
		return new DateTime(updated_at);
	}

	@Override
	public void setUpdatedAt(DateTime updated_at) {
		this.updated_at = updated_at.toGregorianCalendar();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		result = prime * result
				+ ((paciente == null) ? 0 : paciente.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Consulta other = (Consulta) obj;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.equals(other.data))
			return false;
		if (paciente == null) {
			if (other.paciente != null)
				return false;
		} else if (!paciente.equals(other.paciente))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Consulta [id=" + id + ", data=" + data + ", paciente="
				+ paciente + ", active=" + active + ", created_at="
				+ created_at + ", updated_at=" + updated_at + "]";
	}
	
}
