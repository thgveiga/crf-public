package br.com.tveiga.bean;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.joda.time.DateTime;

@Entity
@Table(name="permissao")
public class Permissao implements PersistentEntity {

	@Id
	@GeneratedValue
	private Integer id;
	
	@Column
	private String nome;

	@Column
	private boolean view;

	@Column(name="new")
	private boolean create;

	@Column
	private boolean edit;

	@Column
	private boolean remove;
	
	@Column
	private boolean active;

	@Column
	private Calendar created_at;

	@Column
	private Calendar updated_at;

	@Override
	public Integer getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public boolean isActive() {
		return active;
	}

	@Override
	public void setActive(boolean active) {
		this.active = active;
	}

	@Override
	public DateTime getCreatedAt() {
		return new DateTime(created_at);
	}

	@Override
	public void setCreatedAt(DateTime created_at) {
		this.created_at = created_at.toGregorianCalendar();
	}
	
	@Override
	public DateTime getUpdatedAt() {
		return new DateTime(updated_at);
	}

	@Override
	public void setUpdatedAt(DateTime updated_at) {
		this.updated_at = updated_at.toGregorianCalendar();
	}

	public boolean isView() {
		return view;
	}

	public void setView(boolean view) {
		this.view = view;
	}

	public boolean isCreate() {
		return create;
	}

	public void setCreate(boolean create) {
		this.create = create;
	}

	public boolean isEdit() {
		return edit;
	}

	public void setEdit(boolean edit) {
		this.edit = edit;
	}

	public boolean isRemove() {
		return remove;
	}

	public void setRemove(boolean remove) {
		this.remove = remove;
	}

	@Override
	public String toString() {
		return "Permissao [nome=" + nome + ", view=" + view + ", create="
				+ create + ", edit=" + edit + ", remove=" + remove
				+ ", active=" + active + "]";
	}
	
}
