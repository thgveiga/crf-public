package br.com.tveiga.bean;

import java.util.Calendar;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.joda.time.DateTime;

@Entity
@Table(name="patologia")
public class Patologia implements PersistentEntity{

	@Id
	@GeneratedValue
	private Integer id;
	
	@Column
	private String nome;

	@Column
	private String cid;

	@OneToOne(cascade = CascadeType.ALL, optional = true, fetch = FetchType.EAGER, orphanRemoval = true)
    @JoinColumn(name="disciplina_id", nullable=true)
	private Disciplina disciplina;
	
	@ManyToOne(cascade = CascadeType.ALL, optional = true)
	@JoinColumn(name="grupopatologico_id", nullable=true)
	private GrupoPatologico grupoPatologico;
	
	@Column
	private Calendar created_at;
	
	@Column
	private Calendar updated_at;

	@Column
	private boolean active;

	public Integer getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getCid() {
		return cid;
	}
	
	public void setCid(String cid) {
		this.cid = cid;
	}
	
	public Disciplina getDisciplina() {
		return disciplina;
	}
	
	public void setDisciplina(Disciplina disciplina) {
		this.disciplina = disciplina;
	}
	
	public GrupoPatologico getGrupoPatologico() {
		return grupoPatologico;
	}
	
	public void setGrupoPatologico(GrupoPatologico grupoPatologico) {
		this.grupoPatologico = grupoPatologico;
	}
	
	@Override
	public DateTime getCreatedAt() {
		return new DateTime(created_at);
	}

	@Override
	public void setCreatedAt(DateTime created_at) {
		this.created_at = created_at.toGregorianCalendar();
	}
	
	@Override
	public DateTime getUpdatedAt() {
		return new DateTime(updated_at);
	}

	@Override
	public void setUpdatedAt(DateTime updated_at) {
		this.updated_at = updated_at.toGregorianCalendar();
	}
	
	@Override
	public boolean isActive() {
		return this.active;
	}

	@Override
	public void setActive(boolean active) {
		this.active = active;
	}

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cid == null) ? 0 : cid.hashCode());
		result = prime * result
				+ ((disciplina == null) ? 0 : disciplina.hashCode());
		result = prime * result
				+ ((grupoPatologico == null) ? 0 : grupoPatologico.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Patologia other = (Patologia) obj;
		if (cid == null) {
			if (other.cid != null)
				return false;
		} else if (!cid.equals(other.cid))
			return false;
		if (disciplina == null) {
			if (other.disciplina != null)
				return false;
		} else if (!disciplina.equals(other.disciplina))
			return false;
		if (grupoPatologico == null) {
			if (other.grupoPatologico != null)
				return false;
		} else if (!grupoPatologico.equals(other.grupoPatologico))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return nome;
	}

}
