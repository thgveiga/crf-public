package br.com.tveiga.bean;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

@Entity
@Table(name="examelaboral")
public class ExameLaboral implements PersistentEntity {

	@Id
	@GeneratedValue
	private Integer id;

	@Column
	private String nome;

	@Column
	private boolean active;

	@Column
	private Calendar created_at;

	@Column
	private Calendar updated_at;

	private String descricao;

	private Calendar data;
	
	@Override
	public Integer getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public boolean isActive() {
		return this.active;
	}

	@Override
	public void setActive(boolean active) {
		this.active = active;
	}
	
	@Override
	public DateTime getCreatedAt() {
		return new DateTime(created_at);
	}

	@Override
	public void setCreatedAt(DateTime created_at) {
		this.created_at = created_at.toGregorianCalendar();
	}
	
	@Override
	public DateTime getUpdatedAt() {
		return new DateTime(updated_at);
	}

	@Override
	public void setUpdatedAt(DateTime updated_at) {
		this.updated_at = updated_at.toGregorianCalendar();
	}

	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public DateTime getData() {
		return new DateTime(data);
	}

	public void setData(DateTime data) {
		this.data = data.toGregorianCalendar();
	}
	

}
