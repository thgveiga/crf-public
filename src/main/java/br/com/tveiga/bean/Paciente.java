package br.com.tveiga.bean;

import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.joda.time.DateTime;

@Entity
@Table(name="paciente")
public class Paciente implements PersistentEntity {

	@OneToOne(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	@JoinColumn(name="historico_id", nullable=true)
	private Historico historico;
	 
	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
    @JoinTable(name="consulta_paciente", joinColumns={@JoinColumn(name="paciente_id", referencedColumnName="id")}, inverseJoinColumns={@JoinColumn(name="consulta_id", referencedColumnName="id")})
	private List<Consulta> consultas;
	
	@Id
	@GeneratedValue
	private Integer id;
	
	@Column
	private String nome;

	@Column
	private String same;

	@Column
	private Calendar dataDeAdmissao;
	
	@Column
	private String sexo;
	
	@Column
	private Calendar dataDeNascimento;
	
	@Column
	private Integer idade;
	
	@Column
	private String estadoCivil;
	
	@Column
	private String etnia;
	
	@Column
	private String endereco;
	
	@Column
	private String estado;
	
	@Column
	private Calendar created_at;

	@Column
	private Calendar updated_at;

	@Column
	private boolean active;
	
	@Column
	private String bairro;

	@Column
	private String escolaridade;

	@Column
	private String cep;
	
	@Column
	private String telefoneResidencial;
	
	@Column
	private String telefoneComercial;
	
	@Column
	private String telefoneCelular;
	
	@Column
	private String recados;
	
	@Column
	private String ha;
	
	@Column
	private String procedente;
	
	@Column
	private String profissao;
	
	@Column
	private boolean convenio;
	
	//getters and setters
	
	
	@Override
	public Integer getId() {
		return id;
	}

	public String getRecados() {
		return recados;
	}

	public void setRecados(String recados) {
		this.recados = recados;
	}

	public String getHa() {
		return ha;
	}

	public void setHa(String ha) {
		this.ha = ha;
	}

	public String getProcedente() {
		return procedente;
	}

	public void setProcedente(String procedente) {
		this.procedente = procedente;
	}

	public String getProfissao() {
		return profissao;
	}

	public void setProfissao(String profissao) {
		this.profissao = profissao;
	}

	public String getTelefoneResidencial() {
		return telefoneResidencial;
	}

	public void setTelefoneResidencial(String telefoneResidencial) {
		this.telefoneResidencial = telefoneResidencial;
	}

	public String getTelefoneComercial() {
		return telefoneComercial;
	}

	public void setTelefoneComercial(String telefoneComercial) {
		this.telefoneComercial = telefoneComercial;
	}

	public String getTelefoneCelular() {
		return telefoneCelular;
	}

	public void setTelefoneCelular(String telefoneCelular) {
		this.telefoneCelular = telefoneCelular;
	}

	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getCep() {
		return cep;
	}
	
	public void setCep(String cep) {
		this.cep = cep;
	}
	
	
	public String getSame() {
		return same;
	}

	public void setSame(String same) {
		this.same = same;
	}

	public DateTime getDataDeAdmissao() {
		return new DateTime(created_at);
	}

	public void setDataDeAdmissao(DateTime dataDeAdmissao) {
		this.dataDeAdmissao = dataDeAdmissao.toGregorianCalendar();
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public DateTime getDataDeNascimento() {
		return new DateTime(updated_at);
	}

	public void setDataDeNascimento(DateTime dataDeNascimento) {
		this.dataDeNascimento = dataDeNascimento.toGregorianCalendar();
	}

	public Integer getIdade() {
		return idade;
	}

	public void setIdade(Integer idade) {
		this.idade = idade;
	}

	public String getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public String getEtnia() {
		return etnia;
	}

	public void setEtnia(String etnia) {
		this.etnia = etnia;
	}

	public String getEndereco(){
		return endereco;
	}
	
	public void setEndereco(String endereco){
		this.endereco = endereco;
	}
	
	public String getEstado(){
		return estado;
	}
	
	public void setEstado(String estado){
		this.estado = estado;
	}
	
	public Historico getHistorico() {
		return historico;
	}

	public void setHistorico(Historico historico) {
		this.historico = historico;
	}
	
	public List<Consulta> getConsultas() {
		return consultas;
	}

	public void setConsultas(List<Consulta> consultas) {
		this.consultas = consultas;
	}
	
	@Override
	public DateTime getCreatedAt() {
		return new DateTime(created_at);
	}

	@Override
	public void setCreatedAt(DateTime created_at) {
		this.created_at = created_at.toGregorianCalendar();
	}
	
	@Override
	public DateTime getUpdatedAt() {
		return new DateTime(updated_at);
	}

	@Override
	public void setUpdatedAt(DateTime updated_at) {
		this.updated_at = updated_at.toGregorianCalendar();
	}
	
	@Override
	public boolean isActive() {
		return this.active;
	}

	@Override
	public void setActive(boolean active) {
		this.active = active;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public void setEscolaridade(String escolaridade) {
		this.escolaridade = escolaridade;
	}
	
	public String getEscolaridade() {
		return escolaridade;
	}

	public boolean hasConvenio() {
		return convenio;
	}
	
	public void setConvenio(boolean convenio) {
		this.convenio =  convenio;
	}
}
