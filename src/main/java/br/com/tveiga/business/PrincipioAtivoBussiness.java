package br.com.tveiga.business;

import br.com.tveiga.bean.ClasseTerapeutica;
import br.com.tveiga.bean.PrincipioAtivo;
import br.com.tveiga.dao.PrincipioAtivoDAO;
import br.com.tveiga.factory.EntityDAOFactory;

public final class PrincipioAtivoBussiness extends Business<PrincipioAtivo, PrincipioAtivoDAO>{
	
	
	public PrincipioAtivoBussiness() {
		element = new PrincipioAtivo();
		dao = (PrincipioAtivoDAO)EntityDAOFactory.getEntityDAO(element);
	}
	
	public final ClasseTerapeutica getClasseTerapeutica(String principioAtivoNome){
		element.setActive(true);
		element.setNome(principioAtivoNome);
		PrincipioAtivo principioAtivo = dao.searchOne();
		return principioAtivo.getClasseTerapeutica();
	}

}
