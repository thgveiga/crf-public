package br.com.tveiga.business;

import br.com.tveiga.bean.Patologia;
import br.com.tveiga.dao.PatologiaDAO;
import br.com.tveiga.factory.EntityDAOFactory;

public final class PatologiaBussiness extends Business<Patologia, PatologiaDAO> {
	
	
	public PatologiaBussiness() {
		super.element = new Patologia();
		super.dao = (PatologiaDAO)EntityDAOFactory.getEntityDAO(element);
	}
	
}

