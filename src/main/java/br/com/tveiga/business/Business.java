package br.com.tveiga.business;

import java.util.List;

import br.com.tveiga.bean.PersistentEntity;
import br.com.tveiga.dao.PersistentEntityDAO;
import br.com.tveiga.factory.EntityDAOFactory;
import br.com.tveiga.log.Level;
import br.com.tveiga.log.Log;

public class Business<E extends PersistentEntity, Dao extends PersistentEntityDAO> {

	E element;
	Dao dao;
	
	public boolean saveOrUpdate(PersistentEntity oldElement, PersistentEntity newElement){
		try{
			if(oldElement != null){
				dao = (Dao)EntityDAOFactory.getEntityDAO(oldElement);
				dao.update(newElement);
			}else{
				dao = (Dao)EntityDAOFactory.getEntityDAO(newElement);
				dao.create();
			}
			return true;
		}catch(Exception e){
			Log.log(Business.class.getSimpleName(), 20, e.getMessage(), Level.ERROR);
			return false;
		}
		
	}

	public boolean delete(PersistentEntity persistentEntityCurrent) {
		try{
			dao = (Dao)EntityDAOFactory.getEntityDAO(persistentEntityCurrent);
			dao.delete();
			return true;
		}catch(Exception e){
			Log.log(Business.class.getSimpleName(), 35, e.getMessage(), Level.ERROR);
			return false;
		}
	}
	
	public PersistentEntity previous() {
		dao = (Dao)EntityDAOFactory.getEntityDAO(element);
		element = (E) dao.previous();
		element = element != null ? element : (E)dao.last();
		return element;
	}
	
	public PersistentEntity next() {
		dao = (Dao)EntityDAOFactory.getEntityDAO(element);
		element = (E) dao.next();
		element = element != null ? element : (E)dao.frist();
		return element;
	}
	
	public List<E> getAll(){
		dao = (Dao)EntityDAOFactory.getEntityDAO(element);
		return dao.all();
	}
}
