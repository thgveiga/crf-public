package br.com.tveiga.business;

import br.com.tveiga.bean.PersistentEntity;
import br.com.tveiga.bean.Usuario;
import br.com.tveiga.dao.UsuarioDAO;
import br.com.tveiga.factory.EntityDAOFactory;
import br.com.tveiga.security.key;

public final class UsuarioBusiness extends Business<Usuario, UsuarioDAO> {

	private static Usuario loggedUser;
	
	public static final Usuario getLoggedUser(){
		return UsuarioBusiness.loggedUser;
	}
	
	public UsuarioBusiness() {
		this(null);
	}
	
	public UsuarioBusiness(Usuario usuario) {
		element = usuario != null ? usuario : loggedUser;
	}

	public final boolean login(Usuario usuario){
		usuario.setSenha(key.createPassword(usuario.getSenha()));
		dao = (UsuarioDAO)EntityDAOFactory.getEntityDAO(usuario);
		UsuarioBusiness.loggedUser =  dao.searchOne();
		return UsuarioBusiness.loggedUser != null;
	}
	
	@Override
	public final boolean saveOrUpdate(PersistentEntity oldElement, PersistentEntity newElement){
		Usuario usuarioOld = (Usuario) oldElement;
		Usuario usuarioNew = (Usuario) newElement;
		
		if(usuarioOld == null || !usuarioNew.getSenha().equals(usuarioOld.getSenha()))
			usuarioNew.setSenha(key.createPassword(usuarioNew.getSenha()));
		
		return super.saveOrUpdate(usuarioOld, usuarioNew);
		
	}
	
}
