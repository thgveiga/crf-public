package br.com.tveiga.security;

import java.util.UUID;

import br.com.tveiga.util.Normalize;

public final class key {

	public static final String baseKey = "06d4f2f0-6237-4602-baa1-74273912b0e6";
	
	public static final String generateUniquekey(){
		return UUID.randomUUID().toString();
	}

	public static final String createPassword(String password){
		password = Normalize.normalizeName(password);
		password = password + baseKey;
		password = Normalize.toMD5(password);
		return password;
	}
	
}
