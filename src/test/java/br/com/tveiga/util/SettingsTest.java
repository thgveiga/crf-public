package br.com.tveiga.util;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class SettingsTest {

	private Settings settings;
	
	@Before
	public void init(){
		settings = Settings.getInstance();
	}
	
	@Test
	public void getHost(){
		String host = settings.getPropertie("host");
		Assert.assertEquals("localhost", host);
	}
}
