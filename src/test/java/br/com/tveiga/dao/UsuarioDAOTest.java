package br.com.tveiga.dao;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import br.com.tveiga.bean.Permissao;
import br.com.tveiga.bean.Usuario;
import br.com.tveiga.business.UsuarioBusiness;
import br.com.tveiga.factory.EntityDAOFactory;

public class UsuarioDAOTest {
	
	Usuario usuario;
	UsuarioBusiness usuarioBusiness;
	
	@Before
	public void initObject(){
		usuarioBusiness = new UsuarioBusiness();
		usuario = new Usuario();
		usuario.setNome("teste");
		usuario.setSenha("teste");
		Permissao permissao = new Permissao();
		permissao.setNome("teste");
		permissao.setCreate(true);
		permissao.setUpdatedAt(new DateTime().now());
		permissao.setCreatedAt(new DateTime().now());
		List<Permissao> permissoes = new ArrayList<Permissao>();
		permissoes.add(permissao);
		usuario.setPermissoes(permissoes);
		
	}
	@Test
	public void save() {
		usuarioBusiness.saveOrUpdate(null, usuario);
	}
	
	@Test
	public void all() {
		UsuarioDAO usuarioDAO = (UsuarioDAO)EntityDAOFactory.getEntityDAO(usuario);
		for(Usuario u : usuarioDAO.all())
			System.out.println(u);
	}
	
	@Test
	public void update() {
		Usuario usuario2 = usuario;
		usuario2.setNome("banana");
		usuarioBusiness.saveOrUpdate(usuario, usuario2);
	}
	
	@After
	public void rollback(){
		usuarioBusiness.delete(usuario);
	}

}
